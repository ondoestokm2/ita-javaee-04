package cz.ita.javaee.test;

import javax.naming.NamingException;

import cz.ita.javaee.config.AppConfig;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.mock.jndi.SimpleNamingContextBuilder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { AppConfig.class })
public abstract class SpringContextTest {

    @BeforeClass
    public static void init() throws NamingException {
        // no need shutdown, EmbeddedDatabaseFactoryBean will take care of this
        EmbeddedDatabase database = new EmbeddedDatabaseBuilder()
                .setType(EmbeddedDatabaseType.HSQL) //.H2 or .DERBY
                .addScript("sql/settings.sql")
                .build();

        SimpleNamingContextBuilder builder = new SimpleNamingContextBuilder();
        builder.bind("java:/comp/env/jdbc/itajavaee", database);
        builder.activate();
    }

}
