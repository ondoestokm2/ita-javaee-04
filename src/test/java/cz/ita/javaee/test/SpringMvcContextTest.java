package cz.ita.javaee.test;

import cz.ita.javaee.config.SpringWebConfig;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.web.SpringJUnitWebConfig;

@SpringJUnitWebConfig(classes = {SpringWebConfig.class})
@ActiveProfiles({"jpa"})
public abstract class SpringMvcContextTest extends SpringContextTest {
}