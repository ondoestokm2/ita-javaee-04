package cz.ita.javaee.repository;

import cz.ita.javaee.model.TmDeliveryEntity;
import cz.ita.javaee.model.TmOrderEntity;
import cz.ita.javaee.repository.api.TmDeliveryRepository;
import cz.ita.javaee.test.JpaSpringContextTest;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Transactional
public class JpaTmDeliveryRepositoryTest extends JpaSpringContextTest{

    @Autowired
    private TmDeliveryRepository tmDeliveryRepository;

    @PersistenceContext
    private EntityManager entityManager;

    // find tests

    @Test
    public void testFindAll() {
        final int expectedListSize = 4;

        List<TmDeliveryEntity> entities = tmDeliveryRepository.find(0, Integer.MAX_VALUE);

        Assert.assertEquals(expectedListSize, entities.size());
    }

    @Test
    public void testFindAllWithLimit() {
        final int expectedMaxListSize = 5;

        List<TmDeliveryEntity> entities = tmDeliveryRepository.find(0, expectedMaxListSize);

        Assert.assertThat(entities.size(), Matchers.lessThanOrEqualTo(expectedMaxListSize));
    }

    @Test
    public void testIfFoundedObjectByIdIsNotNull() {
        final long wantedId = 1L;

        TmDeliveryEntity tmDeliveryEntity = tmDeliveryRepository.findOne(wantedId);

        Assert.assertNotNull(tmDeliveryEntity);
    }

    // create tests

    @Test
    public void testCorrectnessOfGeneratedId() {
        final long minimalId = 1;

        TmDeliveryEntity tmDeliveryEntity = new TmDeliveryEntity();
        tmDeliveryEntity.setPeriod(LocalDate.now().plusYears(1));
        tmDeliveryEntity.setCreated(LocalDateTime.now());
        tmDeliveryEntity.setMdNumber(15564);
        TmOrderEntity order = new TmOrderEntity();
        order.setId(2L);
        tmDeliveryEntity.setOrder(order);

        TmDeliveryEntity createdTmDeliveryEntity = tmDeliveryRepository.create(tmDeliveryEntity);

        Assert.assertNotNull(createdTmDeliveryEntity.getId());
        Assert.assertThat(createdTmDeliveryEntity.getId(), Matchers.greaterThanOrEqualTo(minimalId));
    }

    // update tests

    @Test
    public void testUpdatesValues() {
        final long idToFound = 1L;
        TmDeliveryEntity tmDeliveryEntity = tmDeliveryRepository.findOne(idToFound);

        tmDeliveryEntity.setMdNumber(1234);
        tmDeliveryEntity.setPeriod(LocalDate.now().plusYears(1));

        TmDeliveryEntity updatedTmDeliveryEntity = tmDeliveryRepository.update(tmDeliveryEntity);

        Assert.assertEquals(tmDeliveryEntity.getId(), updatedTmDeliveryEntity.getId());
        Assert.assertEquals(tmDeliveryEntity.getPeriod(), updatedTmDeliveryEntity.getPeriod());
        Assert.assertEquals(tmDeliveryEntity.getCreated(), updatedTmDeliveryEntity.getCreated());
    }

    // delete tests

    @Test
    public void testTmDeliveriesCountAfterDelete() {
        final long idToDelete = 1L;

        tmDeliveryRepository.delete(idToDelete);

        long tmDeliveriesCountAfter = entityManager.createQuery("SELECT COUNT(d) FROM TmDeliveryEntity d WHERE d.id = :pId", Long.class)
                .setParameter("pId", idToDelete)
                .getSingleResult();

        Assert.assertEquals(0, tmDeliveriesCountAfter);
    }

    @Test
    public void testIfCorrectTmDeliveryWasDeleted() {
        final long idToDelete = 3L;

        tmDeliveryRepository.delete(idToDelete);

        long deliveriesCount = entityManager.createQuery("SELECT COUNT(d) FROM TmDeliveryEntity d WHERE d.id = :pId", Long.class)
                .setParameter("pId", idToDelete)
                .getSingleResult();

        Assert.assertEquals(0, deliveriesCount);
    }
}
