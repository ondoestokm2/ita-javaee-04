package cz.ita.javaee.repository;

import cz.ita.javaee.model.AddressEntity;
import cz.ita.javaee.model.CompanyEntity;
import cz.ita.javaee.repository.api.CompanyRepository;
import cz.ita.javaee.test.JpaSpringContextTest;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;

@Transactional
public class JpaCompanyRepositoryTest extends JpaSpringContextTest{

    @Autowired
    private CompanyRepository companyRepository;

    @PersistenceContext
    private EntityManager entityManager;

    // find tests

    @Test
    public void testFindAll() {
        final int expectedListSize = 2;

        List<CompanyEntity> entities = companyRepository.find(0, Integer.MAX_VALUE);

        Assert.assertEquals(expectedListSize, entities.size());
    }

    @Test
    public void testFindAllWithLimit() {
        final int expectedMaxListSize = 5;

        List<CompanyEntity> entities = companyRepository.find(0, expectedMaxListSize);

        Assert.assertThat(entities.size(), Matchers.lessThanOrEqualTo(expectedMaxListSize));
    }

    @Test
    public void testIfFoundedObjectByIdIsNotNull() {
        final long wantedId = 1L;

        CompanyEntity companyEntity = companyRepository.findOne(wantedId);

        Assert.assertNotNull(companyEntity);
    }

    // create tests

    @Test
    public void testCorrectnessOfGeneratedId() {
        final long minimalId = 1;

        CompanyEntity companyEntity = new CompanyEntity();
        companyEntity.setName("Test s.r.o");
        companyEntity.setNote("this is note");
        companyEntity.setCompanyId("12345678");
        companyEntity.setOwnershipped(false);
        companyEntity.setVatId("123456789000");
        companyEntity.setCreated(LocalDateTime.now());
        AddressEntity addressEntity = new AddressEntity();
        addressEntity.setCity("Kosice");
        addressEntity.setCountry("SVK");
        addressEntity.setHouseNumber("");
        addressEntity.setStreetName("Dubova");
        addressEntity.setZipCode("00101");
        companyEntity.setAddress(addressEntity);

        CompanyEntity createdCompany = companyRepository.create(companyEntity);

        Assert.assertNotNull(createdCompany.getId());
        Assert.assertThat(createdCompany.getId(), Matchers.greaterThanOrEqualTo(minimalId));
    }

    // update tests

    @Test
    public void testUpdatesValues() {
        final long idToFound = 1L;
        CompanyEntity companyEntity = companyRepository.findOne(idToFound);

        companyEntity.setCompanyId("00000000");
        companyEntity.setName("new name");
        companyEntity.setVatId("XXDSDSD2323");
        companyEntity.setCreated(LocalDateTime.now());
        companyEntity.setNote("XXX note");
        companyEntity.getAddress().setCity("Buanov");
        companyEntity.getAddress().setCountry("SVK");
        companyEntity.getAddress().setHouseNumber("15");
        companyEntity.getAddress().setStreetName("Kysakova");
        companyEntity.getAddress().setZipCode("01101");

        CompanyEntity updatedCompanyEntity = companyRepository.update(companyEntity);

        Assert.assertEquals(companyEntity.getId(), updatedCompanyEntity.getId());
        Assert.assertEquals(companyEntity.getCompanyId(), updatedCompanyEntity.getCompanyId());
        Assert.assertEquals(companyEntity.getName(), updatedCompanyEntity.getName());
        Assert.assertEquals(companyEntity.getVatId(), updatedCompanyEntity.getVatId());
        Assert.assertEquals(companyEntity.getCreated(), updatedCompanyEntity.getCreated());
        Assert.assertEquals(companyEntity.getNote(), updatedCompanyEntity.getNote());
        Assert.assertEquals(companyEntity.getAddress().getCity(), updatedCompanyEntity.getAddress().getCity());
        Assert.assertEquals(companyEntity.getAddress().getCountry(), updatedCompanyEntity.getAddress().getCountry());
        Assert.assertEquals(companyEntity.getAddress().getHouseNumber(), updatedCompanyEntity.getAddress().getHouseNumber());
        Assert.assertEquals(companyEntity.getAddress().getStreetName(), updatedCompanyEntity.getAddress().getStreetName());
        Assert.assertEquals(companyEntity.getAddress().getZipCode(), updatedCompanyEntity.getAddress().getZipCode());
    }

    // delete tests

    @Test
    public void testCompaniesCountAfterDelete() {
        final long idToDelete = 1L;

        companyRepository.delete(idToDelete);

        int companiesAmounAfter = companyRepository.find(0, Integer.MAX_VALUE).size();

        Assert.assertEquals(1, companiesAmounAfter);
    }

    @Test
    public void testIfCorrectCompanyWasDeleted() {
        final long idToDelete = 1L;

        companyRepository.delete(idToDelete);
        List<CompanyEntity> entities = companyRepository.find(0, Integer.MAX_VALUE);
        int amountWithDeletedId = entities.stream().filter(companyEntity -> companyEntity.getId().equals(idToDelete)).toArray().length;

        Assert.assertEquals(amountWithDeletedId, 0);
    }

    @Test
    public void testTmProjectsCountAfterDeletedCustomer() {
        final long idToDelete = 2L;

        companyRepository.delete(idToDelete);
        long tmProjectsCount = entityManager.createQuery("SELECT COUNT(p) FROM TmProjectEntity p WHERE p.customer.id = :pId", Long.class)
                .setParameter("pId", idToDelete)
                .getSingleResult();

        Assert.assertEquals(0, tmProjectsCount);
    }

    @Test
    public void testTmOrdersCountAfterDeletedSupplier() {
        final long idToDelete = 1L;

        companyRepository.delete(idToDelete);
        long tmOrdersCount = entityManager.createQuery("SELECT COUNT(o) FROM TmOrderEntity o WHERE o.supplier.id = :pId", Long.class)
                .setParameter("pId", idToDelete)
                .getSingleResult();

        Assert.assertEquals(0, tmOrdersCount);
    }

    @Test
    public void testTmOrdersCountAfterDeletedSubscriber() {
        final long idToDelete = 1L;

        companyRepository.delete(idToDelete);
        long tmOrdersCount = entityManager.createQuery("SELECT COUNT(o) FROM TmOrderEntity o WHERE o.subscriber.id = :pId", Long.class)
                .setParameter("pId", idToDelete)
                .getSingleResult();
        Assert.assertEquals(0, tmOrdersCount);
    }
}
