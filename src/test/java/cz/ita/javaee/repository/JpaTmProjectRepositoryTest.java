package cz.ita.javaee.repository;

import cz.ita.javaee.model.CompanyEntity;
import cz.ita.javaee.model.TmProjectEntity;
import cz.ita.javaee.repository.api.TmProjectRepository;
import cz.ita.javaee.test.JpaSpringContextTest;
import cz.ita.javaee.type.ProjectStatus;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;

@Transactional
public class JpaTmProjectRepositoryTest extends JpaSpringContextTest{

    @Autowired
    private TmProjectRepository tmProjectRepository;

    @PersistenceContext
    private EntityManager entityManager;

    // find tests

    @Test
    public void testFindAll() {
        final int expectedListSize = 3;

        List<TmProjectEntity> entities = tmProjectRepository.find(0, Integer.MAX_VALUE);

        Assert.assertEquals(expectedListSize, entities.size());
    }

    @Test
    public void testFindAllWithLimit() {
        final int expectedMaxListSize = 5;

        List<TmProjectEntity> entities = tmProjectRepository.find(0, expectedMaxListSize);

        Assert.assertThat(entities.size(), Matchers.lessThanOrEqualTo(expectedMaxListSize));
    }

    @Test
    public void testIfFoundedObjectByIdIsNotNull() {
        final long wantedId = 1L;

        TmProjectEntity tmProjectEntity = tmProjectRepository.findOne(wantedId);

        Assert.assertNotNull(tmProjectEntity);
    }

    // create tests

    @Test
    public void testCorrectnessOfGeneratedId() {
        final long minimalId = 1;

        TmProjectEntity tmProjectEntity = new TmProjectEntity();
        CompanyEntity customer = new CompanyEntity();
        customer.setId(1L);
        tmProjectEntity.setCustomer(customer);
        tmProjectEntity.setName("project name");
        tmProjectEntity.setNote("project note");
        tmProjectEntity.setCreated(LocalDateTime.now());
        tmProjectEntity.setStatus(ProjectStatus.OPEN);

        TmProjectEntity createdTmProjectEntity = tmProjectRepository.create(tmProjectEntity);

        Assert.assertNotNull(createdTmProjectEntity.getId());
        Assert.assertThat(createdTmProjectEntity.getId(), Matchers.greaterThanOrEqualTo(minimalId));
    }

    // update tests

    @Test
    public void testUpdatesValues() {
        final long idToFound = 1L;
        TmProjectEntity tmProjectEntity = tmProjectRepository.findOne(idToFound);

        tmProjectEntity.setName("project name");
        tmProjectEntity.setNote("project note");
        tmProjectEntity.setCreated(LocalDateTime.now());
        tmProjectEntity.setStatus(ProjectStatus.CLOSED);

        TmProjectEntity updatedTmProjectEntity = tmProjectRepository.update(tmProjectEntity);

        Assert.assertEquals(tmProjectEntity.getId(), updatedTmProjectEntity.getId());
        Assert.assertEquals(tmProjectEntity.getName(), updatedTmProjectEntity.getName());
        Assert.assertEquals(tmProjectEntity.getNote(), updatedTmProjectEntity.getNote());
        Assert.assertEquals(tmProjectEntity.getCreated(), updatedTmProjectEntity.getCreated());
        Assert.assertEquals(tmProjectEntity.getStatus(), updatedTmProjectEntity.getStatus());
    }

    // delete tests

    @Test
    public void testTmProjectsCountAfterDelete() {
        final long idToDelete = 2L;

        tmProjectRepository.delete(idToDelete);

        long tmProjectsAmountAfter = entityManager.createQuery("SELECT COUNT(p) FROM TmProjectEntity p", Long.class)
                .getSingleResult();

        Assert.assertEquals(2, tmProjectsAmountAfter);
    }

    @Test
    public void testIfCorrectProjectWasDeleted() {
        final long idToDelete = 3L;

        tmProjectRepository.delete(idToDelete);

        long projectsCount = entityManager.createQuery("SELECT COUNT(p) FROM TmProjectEntity p WHERE p.id = :pId", Long.class)
                .setParameter("pId", idToDelete)
                .getSingleResult();

        Assert.assertEquals(0, projectsCount);
    }

    @Test
    public void testOrdersCountForDeletedProject() {
        final long idToDelete = 1L;

        tmProjectRepository.delete(idToDelete);

        long amountWithDeletedId = entityManager.createQuery("SELECT COUNT(o) FROM TmOrderEntity o WHERE o.project.id = :pId", Long.class)
                .setParameter("pId", idToDelete)
                .getSingleResult();

        Assert.assertEquals(0, amountWithDeletedId);
    }
}
