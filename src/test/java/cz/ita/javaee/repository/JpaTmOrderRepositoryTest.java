package cz.ita.javaee.repository;

import cz.ita.javaee.model.CompanyEntity;
import cz.ita.javaee.model.TmOrderEntity;
import cz.ita.javaee.model.TmProjectEntity;
import cz.ita.javaee.repository.api.TmOrderRepository;
import cz.ita.javaee.test.JpaSpringContextTest;
import cz.ita.javaee.type.TmOrderType;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Transactional
public class JpaTmOrderRepositoryTest extends JpaSpringContextTest{

    @Autowired
    private TmOrderRepository tmOrderRepository;

    @PersistenceContext
    private EntityManager entityManager;

    // find tests

    @Test
    public void testFindAll() {
        final int expectedListSize = 6;

        List<TmOrderEntity> entities = tmOrderRepository.find(0, Integer.MAX_VALUE);

        Assert.assertEquals(expectedListSize, entities.size());
    }

    @Test
    public void testFindAllWithLimit() {
        final int expectedMaxListSize = 5;

        List<TmOrderEntity> entities = tmOrderRepository.find(0, expectedMaxListSize);

        Assert.assertThat(entities.size(), Matchers.lessThanOrEqualTo(expectedMaxListSize));
    }

    @Test
    public void testIfFoundedObjectByIdIsNotNull() {
        final long wantedId = 1L;

        TmOrderEntity tmOrderEntity = tmOrderRepository.findOne(wantedId);

        Assert.assertNotNull(tmOrderEntity);
    }

    // create tests

    @Test
    public void testCorrectnessOfGeneratedId() {
        final long minimalId = 1L;

        TmOrderEntity tmOrderEntity = new TmOrderEntity();

        tmOrderEntity.setMdNumber(15055);
        tmOrderEntity.setMdRate(55);
        tmOrderEntity.setCurrency("EUR");
        tmOrderEntity.setCreated(LocalDateTime.now());
        TmProjectEntity project = new TmProjectEntity();
        project.setId(1L);
        tmOrderEntity.setProject(project);
        CompanyEntity subscriber = new CompanyEntity();
        subscriber.setId(1L);
        tmOrderEntity.setSubscriber(subscriber);
        CompanyEntity supplier = new CompanyEntity();
        supplier.setId(1L);
        tmOrderEntity.setSupplier(supplier);
        tmOrderEntity.setType(TmOrderType.DELIVERY);
        tmOrderEntity.setNumber("XXXX");
        tmOrderEntity.setValidFrom(LocalDate.now());
        tmOrderEntity.setValidTo(LocalDate.now());

        TmOrderEntity createdTmOrderEntity = tmOrderRepository.create(tmOrderEntity);

        Assert.assertNotNull(createdTmOrderEntity.getId());
        Assert.assertThat(createdTmOrderEntity.getId(), Matchers.greaterThanOrEqualTo(minimalId));
    }

    // update tests

    @Test
    public void testUpdatesValues() {
        final long idToFound = 1L;
        TmOrderEntity tmOrderEntity = tmOrderRepository.findOne(idToFound);

        tmOrderEntity.setMdNumber(999);
        tmOrderEntity.setMdRate(99);
        tmOrderEntity.setCurrency("EUR");
        tmOrderEntity.getProject().setId(2L);
        tmOrderEntity.getSubscriber().setId(1L);
        tmOrderEntity.getSupplier().setId(2L);
        tmOrderEntity.setType(TmOrderType.INCOMING);
        tmOrderEntity.setNumber("YYYY");
        tmOrderEntity.setValidFrom(LocalDate.now());
        tmOrderEntity.setValidTo(LocalDate.now());

        TmOrderEntity updatedTmOrderEntity = tmOrderRepository.update(tmOrderEntity);

        Assert.assertEquals(tmOrderEntity.getId(), updatedTmOrderEntity.getId());
        Assert.assertEquals(tmOrderEntity.getMdNumber(), updatedTmOrderEntity.getMdNumber());
        Assert.assertEquals(tmOrderEntity.getMdRate(), updatedTmOrderEntity.getMdRate());
        Assert.assertEquals(tmOrderEntity.getCurrency(), updatedTmOrderEntity.getCurrency());
        Assert.assertEquals(tmOrderEntity.getProject().getId(), updatedTmOrderEntity.getProject().getId());
        Assert.assertEquals(tmOrderEntity.getSubscriber().getId(), updatedTmOrderEntity.getSubscriber().getId());
        Assert.assertEquals(tmOrderEntity.getSupplier().getId(), updatedTmOrderEntity.getSupplier().getId());
        Assert.assertEquals(tmOrderEntity.getType(), updatedTmOrderEntity.getType());
        Assert.assertEquals(tmOrderEntity.getNumber(), updatedTmOrderEntity.getNumber());
        Assert.assertEquals(tmOrderEntity.getValidFrom(), updatedTmOrderEntity.getValidFrom());
        Assert.assertEquals(tmOrderEntity.getValidTo(), updatedTmOrderEntity.getValidTo());
    }

    // delete tests

    @Test
    public void testTmOrdersCountAfterDelete() {
        final long idToDelete = 1L;

        tmOrderRepository.delete(idToDelete);

        long tmOrdersCountAfter = entityManager.createQuery("SELECT COUNT(o) FROM TmOrderEntity o", Long.class)
                .getSingleResult();

        Assert.assertEquals(4, tmOrdersCountAfter);
    }

    @Test
    public void testIfCorrectOrderWasDeleted() {
        final long idToDelete = 3L;

        tmOrderRepository.delete(idToDelete);

        long ordersCount = entityManager.createQuery("SELECT COUNT(o) FROM TmOrderEntity o WHERE o.id = :pId", Long.class)
                .setParameter("pId", idToDelete)
                .getSingleResult();

        Assert.assertEquals(0, ordersCount);
    }

    @Test
    public void testDeliveriesCountForDeletedOrder() {
        final long idToDelete = 1L;

        tmOrderRepository.delete(idToDelete);

        long tmDeliveriesCount = entityManager.createQuery("SELECT COUNT(d) FROM TmDeliveryEntity d WHERE d.order.id = :pId", Long.class)
                .setParameter("pId", idToDelete)
                .getSingleResult();

        Assert.assertEquals(0, tmDeliveriesCount);
    }

}
