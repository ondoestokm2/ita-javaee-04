package cz.ita.javaee.repository;

import cz.ita.javaee.model.AddressEntity;
import cz.ita.javaee.model.EmployeeEntity;
import cz.ita.javaee.repository.api.EmployeeRepository;
import cz.ita.javaee.test.JpaSpringContextTest;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;

@Transactional
public class JpaEmployeeRepositoryTest extends JpaSpringContextTest{

    @Autowired
    private EmployeeRepository employeeRepository;

    @PersistenceContext
    private EntityManager entityManager;

    // find tests

    @Test
    public void testFindAll() {
        final int expectedListSize = 2;

        List<EmployeeEntity> entities = employeeRepository.find(0, Integer.MAX_VALUE);

        Assert.assertEquals(expectedListSize, entities.size());
    }

    @Test
    public void testFindAllWithLimit() {
        final int expectedMaxListSize = 5;

        List<EmployeeEntity> entities = employeeRepository.find(0, expectedMaxListSize);

        Assert.assertThat(entities.size(), Matchers.lessThanOrEqualTo(expectedMaxListSize));
    }

    @Test
    public void testIfFoundedObjectByIdIsNotNull() {
        final long wantedId = 3L;

        EmployeeEntity employeeEntity = employeeRepository.findOne(wantedId);

        Assert.assertNotNull(employeeEntity);
    }

    // create tests

    @Test
    public void testCorrectnessOfGeneratedId() {
        final long minimalId = 1;

        EmployeeEntity employeeEntity = new EmployeeEntity();
        employeeEntity.setFirstName("Gejza");
        employeeEntity.setSurname("Boborovsky");
        employeeEntity.setCreated(LocalDateTime.now());
        employeeEntity.setNote("XXX");
        AddressEntity addressEntity = new AddressEntity();
        addressEntity.setCity("Kosice");
        addressEntity.setCountry("SVK");
        addressEntity.setHouseNumber("");
        addressEntity.setStreetName("Dubova");
        addressEntity.setZipCode("00101");
        employeeEntity.setAddress(addressEntity);

        EmployeeEntity createdEmployee = employeeRepository.create(employeeEntity);

        Assert.assertNotNull(createdEmployee.getId());
        Assert.assertThat(createdEmployee.getId(), Matchers.greaterThanOrEqualTo(minimalId));
    }

    // update tests

    @Test
    public void testUpdatesValues() {
        final long idToFound = 3L;
        EmployeeEntity employeeEntity = employeeRepository.findOne(idToFound);

        employeeEntity.setFirstName("Gejza");
        employeeEntity.setSurname("Boborovsky");
        employeeEntity.setCreated(LocalDateTime.now());
        employeeEntity.setNote("XXX");
        employeeEntity.getAddress().setCity("Kosice");
        employeeEntity.getAddress().setCountry("SVK");
        employeeEntity.getAddress().setHouseNumber("");
        employeeEntity.getAddress().setStreetName("Dubova");
        employeeEntity.getAddress().setZipCode("00101");

        EmployeeEntity updatedEmployeeEntity = employeeRepository.update(employeeEntity);

        Assert.assertEquals(employeeEntity.getId(), updatedEmployeeEntity.getId());
        Assert.assertEquals(employeeEntity.getFirstName(), updatedEmployeeEntity.getFirstName());
        Assert.assertEquals(employeeEntity.getSurname(), updatedEmployeeEntity.getSurname());
        Assert.assertEquals(employeeEntity.getNote(), updatedEmployeeEntity.getNote());
        Assert.assertEquals(employeeEntity.getAddress().getCity(), updatedEmployeeEntity.getAddress().getCity());
        Assert.assertEquals(employeeEntity.getAddress().getCountry(), updatedEmployeeEntity.getAddress().getCountry());
        Assert.assertEquals(employeeEntity.getAddress().getHouseNumber(), updatedEmployeeEntity.getAddress().getHouseNumber());
        Assert.assertEquals(employeeEntity.getAddress().getStreetName(), updatedEmployeeEntity.getAddress().getStreetName());
        Assert.assertEquals(employeeEntity.getAddress().getZipCode(), updatedEmployeeEntity.getAddress().getZipCode());

    }

    @Test
    public void testEmployeesCountAfterDelete() {
        final long idToDelete = 3L;

        employeeRepository.delete(idToDelete);

        int employeesCountAfter = employeeRepository.find(0, Integer.MAX_VALUE).size();

        Assert.assertEquals(1, employeesCountAfter);
    }

    // delete tests

    @Test
    public void testIfCorrectEmployeeWasDeleted() {
        final long idToDelete = 3L;

        employeeRepository.delete(idToDelete);

        long employeesCount = entityManager.createQuery("SELECT COUNT(e) FROM EmployeeEntity e WHERE e.id = :pId", Long.class)
                .setParameter("pId", idToDelete)
                .getSingleResult();

        Assert.assertEquals(0, employeesCount);
    }
}
