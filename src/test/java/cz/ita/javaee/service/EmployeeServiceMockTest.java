package cz.ita.javaee.service;

import cz.ita.javaee.dto.EmployeeDto;
import cz.ita.javaee.util.Translations;
import cz.ita.javaee.matcher.EmployeeMatcher;
import cz.ita.javaee.model.AddressEntity;
import cz.ita.javaee.model.EmployeeEntity;
import cz.ita.javaee.repository.api.EmployeeRepository;
import cz.ita.javaee.service.api.EmployeeService;
import cz.ita.javaee.service.api.NotificationService;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class EmployeeServiceMockTest {

    private final Long FIRST_EMPLOYEE_ID = 1L;

    @Mock
    private EmployeeRepository employeeRepository;

    @Mock
    private NotificationService notificationService;

    @InjectMocks
    private EmployeeService employeeService = new EmployeeServiceImpl();

    private ArrayList<EmployeeEntity> employees;

    @Before
    public void setUp() {
        employees = new ArrayList<>();
        EmployeeEntity employeeEntity1 = new EmployeeEntity();
        employeeEntity1.setId(1L);
        employeeEntity1.setFirstName("Fero");
        employeeEntity1.setSurname("Mrkvicka");
        employeeEntity1.setActive(true);
        employeeEntity1.setNote("Je to borec");
        employeeEntity1.setCreated(LocalDateTime.now());
        AddressEntity addressEntity1 = new AddressEntity();
        addressEntity1.setStreetName("Lidická");
        addressEntity1.setZipCode("60200");
        addressEntity1.setCity("Brno");
        addressEntity1.setHouseNumber("12");
        addressEntity1.setCountry("CZE");
        employeeEntity1.setAddress(addressEntity1);

        EmployeeEntity employeeEntity2 = new EmployeeEntity();
        employeeEntity2.setId(2L);
        employeeEntity2.setFirstName("Antonin");
        employeeEntity2.setSurname("Novak");
        employeeEntity2.setActive(true);
        employeeEntity2.setNote("Manazeris");
        employeeEntity2.setCreated(LocalDateTime.now());
        AddressEntity addressEntity2 = new AddressEntity();
        addressEntity2.setStreetName("Farska");
        addressEntity2.setZipCode("01898");
        addressEntity2.setCity("Stropkov");
        addressEntity2.setHouseNumber("1/a");
        addressEntity2.setCountry("SVK");
        employeeEntity2.setAddress(addressEntity2);

        employees.add(employeeEntity1);
        employees.add(employeeEntity2);
    }

    // find tests

    @Test
    public void testFindAllCollectionSize() {
        Mockito.when(employeeRepository.find(0, Integer.MAX_VALUE)).thenReturn(employees);

        List<EmployeeDto> employeeDtos = employeeService.findAll();

        Assert.assertThat(employeeDtos, Matchers.hasSize(2));
    }

    @Test
    public void testFindAllPropertiesValues() {
        Mockito.when(employeeRepository.find(0, Integer.MAX_VALUE)).thenReturn(employees);

        List<EmployeeDto> employeeDtos = employeeService.findAll();

        Assert.assertThat(employees, Matchers.hasItems(
                new EmployeeMatcher(Translations.EMPLOYEE_DTO_TO_DOMAIN.apply(employeeDtos.get(0))),
                new EmployeeMatcher(Translations.EMPLOYEE_DTO_TO_DOMAIN.apply(employeeDtos.get(1)))
        ));
    }

    // create tests

    @Test
    public void testCreateWithInitializedObject() {

        Mockito.when(employeeRepository.create(Mockito.any(EmployeeEntity.class))).thenReturn(employees.get(0));
        EmployeeDto toCreate = Translations.EMPLOYEE_DOMAIN_TO_DTO.apply(employees.get(0));
        toCreate.setId(null);

        EmployeeDto created = employeeService.create(toCreate);

        Assert.assertThat(created.getId(), Matchers.greaterThanOrEqualTo(1L));
        Mockito.verify(notificationService).sendNotifiction(Mockito.anyString(), Mockito.anyString());
    }

    @Test
    public void testCreateWithNull() {

        Mockito.when(employeeRepository.create(Mockito.isNull())).thenReturn(null);
        EmployeeDto created = employeeService.create(null);

        Assert.assertNull(created);
        Mockito.verify(notificationService, Mockito.never()).sendNotifiction(Mockito.anyString(), Mockito.anyString());
    }

    // update tests

    @Test
    public void testRepositoryUpdate() {
        Mockito.when(employeeRepository.update(Mockito.any(EmployeeEntity.class))).thenReturn(employees.get(0));

        employeeService.update(Translations.EMPLOYEE_DOMAIN_TO_DTO.apply(employees.get(0)));

        Mockito.verify(employeeRepository).update(Mockito.any(EmployeeEntity.class));
    }

    // delete tests

    @Test
    public void testRepositoryDelete() {
        Mockito.doNothing().when(employeeRepository).delete(Mockito.anyLong());

        employeeService.delete(FIRST_EMPLOYEE_ID);

        Mockito.verify(employeeRepository).delete(FIRST_EMPLOYEE_ID);
    }
}
