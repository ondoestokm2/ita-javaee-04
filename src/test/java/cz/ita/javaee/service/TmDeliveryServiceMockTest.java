package cz.ita.javaee.service;

import cz.ita.javaee.dto.TmDeliveryDto;
import cz.ita.javaee.dto.TmOrderDto;
import cz.ita.javaee.util.Translations;
import cz.ita.javaee.model.TmDeliveryEntity;
import cz.ita.javaee.repository.api.TmDeliveryRepository;
import cz.ita.javaee.service.api.NotificationService;
import cz.ita.javaee.service.api.TmDeliveryService;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.LocalDate;
import java.time.LocalDateTime;

@RunWith(MockitoJUnitRunner.class)
public class TmDeliveryServiceMockTest {

    public final Long FIRST_TM_DELIVERY_ID = 1L;

    @Mock
    private TmDeliveryRepository tmDeliveryRepository;

    @Mock
    private NotificationService notificationService;

    @InjectMocks
    private TmDeliveryService tmDeliveryService = new TmDeliveryServiceImpl();

    private TmDeliveryDto tmDeliveryDto;

    @Before
    public void setUp() {
        tmDeliveryDto = new TmDeliveryDto();
        tmDeliveryDto.setId(1L);
        tmDeliveryDto.setCreated(LocalDateTime.now());
        tmDeliveryDto.setMdNumber(1500);
        tmDeliveryDto.setPeriod(LocalDate.now());
        tmDeliveryDto.setOrder(new TmOrderDto());
    }

    // create tests

    @Test
    public void testCreateWithInitializedObject() {

        Mockito.when(tmDeliveryRepository.create(Mockito.any(TmDeliveryEntity.class))).thenReturn(Translations.TM_DELIVERY_DTO_TO_DOMAIN.apply(tmDeliveryDto));

        tmDeliveryDto.setId(null);

        TmDeliveryDto created = tmDeliveryService.create(tmDeliveryDto);

        Assert.assertThat(created.getId(), Matchers.greaterThanOrEqualTo(1L));
        Mockito.verify(notificationService).sendNotifiction(Mockito.anyString(), Mockito.anyString());
    }

    @Test
    public void testCreateWithNull() {

        Mockito.when(tmDeliveryRepository.create(Mockito.isNull())).thenReturn(null);
        TmDeliveryDto created = tmDeliveryService.create(null);

        Assert.assertNull(created);
        Mockito.verify(notificationService, Mockito.never()).sendNotifiction(Mockito.anyString(), Mockito.anyString());
    }

    // update tests

    @Test
    public void testRepositoryUpdate() {
        Mockito.when(tmDeliveryRepository.update(Mockito.any(TmDeliveryEntity.class))).thenReturn(Translations.TM_DELIVERY_DTO_TO_DOMAIN.apply(tmDeliveryDto));

        tmDeliveryService.update(tmDeliveryDto);

        Mockito.verify(tmDeliveryRepository).update(Mockito.any(TmDeliveryEntity.class));
    }

    // delete tests

    @Test
    public void testRepositoryDelete() {
        Mockito.doNothing().when(tmDeliveryRepository).delete(Mockito.anyLong());

        tmDeliveryService.delete(FIRST_TM_DELIVERY_ID);

        Mockito.verify(tmDeliveryRepository).delete(FIRST_TM_DELIVERY_ID);
    }
}
