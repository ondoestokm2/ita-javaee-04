package cz.ita.javaee.service;

import cz.ita.javaee.dto.CompanyDto;
import cz.ita.javaee.service.api.AresCompanyService;
import cz.ita.javaee.test.JpaSpringContextTest;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Assume;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.test.client.MockWebServiceServer;
import org.springframework.xml.transform.StringSource;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import static org.springframework.ws.test.client.RequestMatchers.payload;
import static org.springframework.ws.test.client.ResponseCreators.withPayload;

public class AresCompanyServiceMockTest extends JpaSpringContextTest {

    @Autowired
    private AresCompanyService aresCompanyService;

    @Autowired
    private WebServiceTemplate webServiceTemplate;

    private MockWebServiceServer mockServer;

    private String validRequest;
    private String incorrectMaxRecordsRequest;
    private String validResponse;
    private String incorrectMaxRecordsResponse;

    @Before
    public void setUp() throws Exception {
        Path requestPath = Paths.get(getClass().getClassLoader()
                .getResource("ares-soap/ares_request.xml").toURI());
        Path incorrectMaxRecordsRequestPath = Paths.get(getClass().getClassLoader()
                .getResource("ares-soap/ares_max_records_request.xml").toURI());
        Path responsePath = Paths.get(getClass().getClassLoader()
                .getResource("ares-soap/ares_response.xml").toURI());
        Path incorrectMaxRecordsResponsePath = Paths.get(getClass().getClassLoader()
                .getResource("ares-soap/ares_max_records_response.xml").toURI());

        validRequest = new String(Files.readAllBytes(requestPath));
        validResponse = new String(Files.readAllBytes(responsePath));

        incorrectMaxRecordsRequest = new String(Files.readAllBytes(incorrectMaxRecordsRequestPath));
        incorrectMaxRecordsResponse = new String(Files.readAllBytes(incorrectMaxRecordsResponsePath));


        mockServer = MockWebServiceServer.createServer(webServiceTemplate);
    }

    @Test
    public void testValidRequest() throws Exception {

        Assume.assumeNotNull(aresCompanyService);
        Assume.assumeNotNull(webServiceTemplate);
        Assume.assumeNotNull(mockServer);
        Assume.assumeNotNull(validRequest);
        Assume.assumeNotNull(validResponse);

        mockServer.expect(payload(new StringSource(validRequest)))
                .andRespond(withPayload(new StringSource(validResponse)));

        List<CompanyDto> companies = aresCompanyService.findCompany("smartbrains");

        Assert.assertThat(companies, Matchers.hasSize(2));

        Assert.assertEquals("Smartbrains solutions s.r.o.", companies.get(0).getName());
        Assert.assertEquals("04380011", companies.get(0).getCompanyId());
        Assert.assertEquals("Brno", companies.get(0).getAddress().getCity());
        Assert.assertEquals("Lidická", companies.get(0).getAddress().getStreetName());
        Assert.assertEquals("700", companies.get(0).getAddress().getHouseNumber());
        Assert.assertEquals("60200", companies.get(0).getAddress().getZipCode());
        Assert.assertEquals("CZE", companies.get(0).getAddress().getCountry());

        Assert.assertEquals("Smartbrains group s.r.o.", companies.get(1).getName());
        Assert.assertEquals("06561080", companies.get(1).getCompanyId());
        Assert.assertEquals("Brno", companies.get(1).getAddress().getCity());
        Assert.assertEquals("Lidická", companies.get(0).getAddress().getStreetName());
        Assert.assertEquals("700", companies.get(1).getAddress().getHouseNumber());
        Assert.assertEquals("60200", companies.get(1).getAddress().getZipCode());
        Assert.assertEquals("CZE", companies.get(1).getAddress().getCountry());

        mockServer.verify();
    }

    @Test(expected = Exception.class)
    public void testIncorrectRecordsNumberRequest() throws Exception {

        Assume.assumeNotNull(aresCompanyService);
        Assume.assumeNotNull(webServiceTemplate);
        Assume.assumeNotNull(mockServer);
        Assume.assumeNotNull(incorrectMaxRecordsRequest);
        Assume.assumeNotNull(incorrectMaxRecordsResponse);

        mockServer.expect(payload(new StringSource(incorrectMaxRecordsRequest)))
                .andRespond(withPayload(new StringSource(incorrectMaxRecordsResponse)));

        List<CompanyDto> companies = aresCompanyService.findCompany("smart");

        mockServer.verify();
    }


}
