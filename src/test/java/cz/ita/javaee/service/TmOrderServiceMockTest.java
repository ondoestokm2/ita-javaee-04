package cz.ita.javaee.service;

import cz.ita.javaee.dto.*;
import cz.ita.javaee.model.TmOrderEntity;
import cz.ita.javaee.repository.api.TmOrderRepository;
import cz.ita.javaee.service.api.NotificationService;
import cz.ita.javaee.service.api.TmOrderService;
import cz.ita.javaee.type.TmOrderType;
import cz.ita.javaee.util.Translations;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.LocalDate;
import java.util.HashSet;

@RunWith(MockitoJUnitRunner.class)
public class TmOrderServiceMockTest {

    public final Long FIRST_TM_ORDER_ID = 1L;

    @Mock
    private TmOrderRepository tmOrderRepository;

    @Mock
    private NotificationService notificationService;

    @InjectMocks
    TmOrderService tmOrderService = new TmOrderServiceImpl();

    private TmOrderDto order;

    @Before
    public void setUp() {
        order = new TmOrderDto();
        order.setId(1L);
        order.setMdRate(50);
        order.setMdNumber(8000);
        order.setType(TmOrderType.INCOMING);
        order.setMainOrder(new TmOrderDto());
        order.getMainOrder().setId(1L);
        order.setValidTo(LocalDate.now());
        order.setValidTo(LocalDate.now());
        order.setNumber("12345678");
        CompanyDto supplier = new CompanyDto();
        supplier.setId(1L);
        supplier.setAddress(new AddressDto());
        order.setSupplier(supplier);
        CompanyDto subscriber = new CompanyDto();
        subscriber.setId(1L);
        subscriber.setAddress(new AddressDto());
        order.setSubscriber(subscriber);
        order.getSubscriber().setId(1L);
        order.setSubOrders(new HashSet<>());
        order.setCurrency("EUR");
        order.setDeliveries(new HashSet<>());
        order.setProject(new TmProjectDto());
        order.getProject().setId(1L);
    }

   // create tests

    @Test
    public void testCreateWithInitializedObject() {

        Mockito.when(tmOrderRepository.create(Mockito.any(TmOrderEntity.class))).thenAnswer(invocation -> {
            TmOrderEntity order = invocation.getArgument(0);
            order.setId(3L);
            return order;
        });

        order.setId(null);

        TmOrderDto created = tmOrderService.create(order);

        Assert.assertThat(created.getId(), Matchers.greaterThanOrEqualTo(1L));
        Mockito.verify(notificationService).sendNotifiction(Mockito.anyString(), Mockito.anyString());
    }

    @Test
    public void testCreateWithNull() {

        Mockito.when(tmOrderRepository.create(Mockito.isNull())).thenReturn(null);
        TmOrderDto created = tmOrderService.create(null);

        Assert.assertNull(created);
        Mockito.verify(notificationService, Mockito.never()).sendNotifiction(Mockito.anyString(), Mockito.anyString());
    }

    // update tests

    @Test
    public void testRepositoryUpdate() {
        Mockito.when(tmOrderRepository.update(Mockito.any(TmOrderEntity.class))).thenReturn(Translations.TM_ORDER_DTO_TO_DOMAIN.apply(order));

        tmOrderService.update(order);

        Mockito.verify(tmOrderRepository).update(Mockito.any(TmOrderEntity.class));
    }

    // delete tests

    @Test
    public void testRepositoryDelete() {
        Mockito.doNothing().when(tmOrderRepository).delete(Mockito.anyLong());

        tmOrderService.delete(FIRST_TM_ORDER_ID);

        Mockito.verify(tmOrderRepository).delete(FIRST_TM_ORDER_ID);
    }
}
