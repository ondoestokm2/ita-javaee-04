package cz.ita.javaee.service;

import cz.ita.javaee.dto.TmProjectDto;
import cz.ita.javaee.util.Translations;
import cz.ita.javaee.matcher.TmProjectMatcher;
import cz.ita.javaee.model.AddressEntity;
import cz.ita.javaee.model.CompanyEntity;
import cz.ita.javaee.model.TmProjectEntity;
import cz.ita.javaee.repository.api.TmProjectRepository;
import cz.ita.javaee.service.api.NotificationService;
import cz.ita.javaee.service.api.TmProjectService;
import cz.ita.javaee.type.ProjectStatus;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class TmProjectServiceMockTest {

    public final Long FIRST_TM_PROJECT_ID = 1L;

    @Mock
    private TmProjectRepository tmProjectRepository;

    @Mock
    private NotificationService notificationService;

    @InjectMocks
    TmProjectService tmProjectService = new TmProjectServiceImpl();

    private ArrayList<TmProjectEntity> projects;

    @Before
    public void setUp() {
        projects = new ArrayList<>();
        TmProjectEntity project1 = new TmProjectEntity();
        project1.setId(1L);
        CompanyEntity customer1 = new CompanyEntity();
        customer1.setId(1L);
        customer1.setAddress(new AddressEntity());
        project1.setCustomer(customer1);
        project1.setName("Super Project 1");
        project1.setStatus(ProjectStatus.OPEN);
        project1.setOrders(new HashSet<>());
        project1.setNote("Pre NASA");
        project1.setCreated(LocalDateTime.now());

        TmProjectEntity project2 = new TmProjectEntity();
        project2.setId(2L);
        CompanyEntity customer2 = new CompanyEntity();
        customer2.setId(1L);
        customer2.setAddress(new AddressEntity());
        project2.setCustomer(customer2);
        project2.setName("Super Project 2");
        project2.setStatus(ProjectStatus.OPEN);
        project2.setOrders(new HashSet<>());
        project2.setNote("Pre Cinu");
        project2.setCreated(LocalDateTime.now());

        projects.add(project1);
        projects.add(project2);
    }

    // find tests

    @Test
    public void testFindAllCollectionSize() {
        Mockito.when(tmProjectRepository.find(0, Integer.MAX_VALUE)).thenReturn(projects);

        List<TmProjectDto> tmProjectDtos = tmProjectService.findAll();

        Assert.assertThat(tmProjectDtos, Matchers.hasSize(2));
    }

    @Test
    public void testFindAllPropertiesValues() {
        Mockito.when(tmProjectRepository.find(0, Integer.MAX_VALUE)).thenReturn(projects);

        List<TmProjectDto> tmProjectDtos = tmProjectService.findAll();

        Assert.assertThat(projects, Matchers.hasItems(
                new TmProjectMatcher(Translations.TM_PROJECT_DTO_TO_DOMAIN.apply(tmProjectDtos.get(0))),
                new TmProjectMatcher(Translations.TM_PROJECT_DTO_TO_DOMAIN.apply(tmProjectDtos.get(1)))
        ));
    }

    // create tests

    @Test
    public void testCreateWithInitializedObject() {

        Mockito.when(tmProjectRepository.create(Mockito.any(TmProjectEntity.class))).thenReturn(projects.get(0));
        TmProjectDto toCreate = Translations.TM_PROJECT_DOMAIN_TO_DTO.apply(projects.get(0));
        toCreate.setId(null);

        TmProjectDto created = tmProjectService.create(toCreate);

        Assert.assertThat(created.getId(), Matchers.greaterThanOrEqualTo(1L));
        Mockito.verify(notificationService).sendNotifiction(Mockito.anyString(), Mockito.anyString());
    }

    @Test
    public void testCreateWithNull() {

        Mockito.when(tmProjectRepository.create(Mockito.isNull())).thenReturn(null);
        TmProjectDto created = tmProjectService.create(null);

        Assert.assertNull(created);
        Mockito.verify(notificationService, Mockito.never()).sendNotifiction(Mockito.anyString(), Mockito.anyString());
    }

    // update tests

    @Test
    public void testRepositoryUpdate() {
        Mockito.when(tmProjectRepository.update(Mockito.any(TmProjectEntity.class))).thenReturn(projects.get(0));

        tmProjectService.update(Translations.TM_PROJECT_DOMAIN_TO_DTO.apply(projects.get(0)));

        Mockito.verify(tmProjectRepository).update(Mockito.any(TmProjectEntity.class));
    }

    // delete tests

    @Test
    public void testRepositoryDelete() {
        Mockito.doNothing().when(tmProjectRepository).delete(Mockito.anyLong());

        tmProjectService.delete(FIRST_TM_PROJECT_ID);

        Mockito.verify(tmProjectRepository).delete(FIRST_TM_PROJECT_ID);
    }
}
