package cz.ita.javaee.service;

import cz.ita.javaee.service.api.NotificationService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;

@RunWith(MockitoJUnitRunner.class)
public class NotificationServiceMockTest {

    @Mock
    private JavaMailSender mailSender;

    @Captor
    private ArgumentCaptor<SimpleMailMessage> captor;

    @InjectMocks
    private NotificationService notificationService = new NotificationServiceImpl();

    @Test
    public void testMailSender() {
        notificationService.sendNotifiction("Title", "Text");

        Mockito.verify(mailSender).send(captor.capture());
        Assert.assertEquals("Title", captor.getValue().getSubject());
        Assert.assertEquals("Text", captor.getValue().getText());
    }
}
