package cz.ita.javaee.service;

import cz.ita.javaee.dto.CompanyDto;
import cz.ita.javaee.util.Translations;
import cz.ita.javaee.matcher.CompanyMatcher;
import cz.ita.javaee.model.AddressEntity;
import cz.ita.javaee.model.CompanyEntity;
import cz.ita.javaee.repository.api.CompanyRepository;
import cz.ita.javaee.service.api.CompanyService;
import cz.ita.javaee.service.api.NotificationService;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class CompanyServiceMockTest {

    private final Long FIRST_COMPANY_ID = 1L;

    @Mock
    private CompanyRepository companyRepository;

    @Mock
    private NotificationService notificationService;

    @InjectMocks
    CompanyService companyService = new CompanyServiceImpl();

    private ArrayList<CompanyEntity> companies;

    @Before
    public void setUp() {
        companies = new ArrayList<>();
        CompanyEntity company1 = new CompanyEntity();
        company1.setId(1L);
        company1.setName("Best soft s.r.o.");
        company1.setCompanyId("11223344");
        company1.setVatId("CY11223344");
        company1.setActive(true);
        company1.setOwnershipped(true);
        company1.setNote("Hlavní firma.");
        company1.setCreated(LocalDateTime.now());
        AddressEntity addressCompany1 = new AddressEntity();
        addressCompany1.setStreetName("Lidická");
        addressCompany1.setZipCode("60200");
        addressCompany1.setCity("Brno");
        addressCompany1.setHouseNumber("12");
        addressCompany1.setCountry("CZE");
        company1.setAddress(addressCompany1);

        CompanyEntity company2 = new CompanyEntity();
        company2.setId(2L);
        company2.setName("Wekolo");
        company2.setCompanyId("11112222");
        company2.setVatId("SR11112222");
        company2.setActive(true);
        company2.setOwnershipped(false);
        company1.setNote("Hlavní firma 2.");
        company2.setCreated(LocalDateTime.now());
        AddressEntity addressCompany2 = new AddressEntity();
        addressCompany2.setStreetName("Farska");
        addressCompany2.setZipCode("01898");
        addressCompany2.setCity("Stropkov");
        addressCompany2.setHouseNumber("1/a");
        addressCompany2.setCountry("SVK");
        company2.setAddress(addressCompany2);

        companies.add(company1);
        companies.add(company2);
    }

    // find tests

    @Test
    public void testFindAllCollectionSize() {
        Mockito.when(companyRepository.find(0, Integer.MAX_VALUE)).thenReturn(companies);

        List<CompanyDto> companyDtos = companyService.findAll();

        Assert.assertThat(companyDtos, Matchers.hasSize(2));
    }

    @Test
    public void testFindAllPropertiesValues() {
        Mockito.when(companyRepository.find(0, Integer.MAX_VALUE)).thenReturn(companies);

        List<CompanyDto> companyDtos = companyService.findAll();
        Assert.assertThat(companies, Matchers.hasItems(
                new CompanyMatcher(Translations.COMPANY_DTO_TO_DOMAIN.apply(companyDtos.get(0))),
                new CompanyMatcher(Translations.COMPANY_DTO_TO_DOMAIN.apply(companyDtos.get(1)))
        ));
    }

    // create tests

    @Test
    public void testCreateWithInitializedObject() {

        Mockito.when(companyRepository.create(Mockito.any(CompanyEntity.class))).thenReturn(companies.get(0));
        CompanyDto toCreate = Translations.COMPANY_DOMAIN_TO_DTO.apply(companies.get(0));
        toCreate.setId(null);

        CompanyDto created = companyService.create(toCreate);

        Assert.assertThat(created.getId(), Matchers.greaterThanOrEqualTo(1L));
        Mockito.verify(notificationService).sendNotifiction(Mockito.anyString(), Mockito.anyString());
    }

    @Test
    public void testCreateWithNull() {
        Mockito.when(companyRepository.create(Mockito.isNull())).thenReturn(null);
        CompanyDto created = companyService.create(null);

        Assert.assertNull(created);
        Mockito.verify(notificationService, Mockito.never()).sendNotifiction(Mockito.anyString(), Mockito.anyString());
    }

    // update tests

    @Test
    public void testRepositoryUpdate() {
        Mockito.when(companyRepository.update(Mockito.any(CompanyEntity.class))).thenReturn(companies.get(0));

        companyService.update(Translations.COMPANY_DOMAIN_TO_DTO.apply(companies.get(0)));

        Mockito.verify(companyRepository).update(Mockito.any(CompanyEntity.class));
    }

    // delete tests

    @Test
    public void testRepositoryDelete() {
        Mockito.doNothing().when(companyRepository).delete(Mockito.anyLong());

        companyService.delete(FIRST_COMPANY_ID);

        Mockito.verify(companyRepository).delete(FIRST_COMPANY_ID);
    }
}
