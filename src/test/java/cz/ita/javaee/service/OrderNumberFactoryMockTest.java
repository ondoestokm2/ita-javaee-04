package cz.ita.javaee.service;

import cz.ita.javaee.dto.TmOrderDto;
import cz.ita.javaee.model.TmOrderEntity;
import cz.ita.javaee.repository.api.TmOrderRepository;
import cz.ita.javaee.service.api.OrderNumberFactory;
import cz.ita.javaee.service.api.TmOrderService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.LocalDateTime;

@RunWith(MockitoJUnitRunner.class)
public class OrderNumberFactoryMockTest {

    @Mock
    private TmOrderService tmOrderService;

    @InjectMocks
    private OrderNumberFactory orderNumberFactory = new OrderNumberFactoryImpl();

    @Test
    public void testCreationDeliveryOrderNumberWithoutPreviousRecords() {

        Mockito.when(tmOrderService.findLastDeliveryOrder(2017)).thenReturn(null);

        String generatedNumber = orderNumberFactory.getOrderNumber();

        Assert.assertEquals("20170001", generatedNumber);
    }

    @Test
    public void testCreationDeliveryOrderNumberWithPreviousRecords() {

        TmOrderDto tmOrderEntity = new TmOrderDto();
        tmOrderEntity.setNumber("20170158");
        Mockito.when(tmOrderService.findLastDeliveryOrder(LocalDateTime.now().getYear())).thenReturn(tmOrderEntity);

        String generatedNumber = orderNumberFactory.getOrderNumber();

        Assert.assertEquals("20170159", generatedNumber);
    }
}
