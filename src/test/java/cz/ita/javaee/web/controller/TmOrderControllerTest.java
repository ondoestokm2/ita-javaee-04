package cz.ita.javaee.web.controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import cz.ita.javaee.dto.AddressDto;
import cz.ita.javaee.dto.CompanyDto;
import cz.ita.javaee.dto.TmOrderDto;
import cz.ita.javaee.dto.TmProjectDto;
import cz.ita.javaee.service.api.TmOrderService;
import cz.ita.javaee.test.SpringMvcContextTest;
import cz.ita.javaee.type.ProjectStatus;
import cz.ita.javaee.type.TmOrderType;
import cz.ita.javaee.web.controller.error.ControllerExceptionHandler;
import cz.ita.javaee.web.controller.util.LocalDateAdapter;
import cz.ita.javaee.web.controller.util.LocalDateTimeAdapter;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import javax.persistence.EntityNotFoundException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashSet;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class TmOrderControllerTest extends SpringMvcContextTest {

    private final String API_URL = "/api/tm-order";
    private MockMvc mockMvc;
    private TmOrderDto tmOrderDto;
    private Gson gson;

    @Mock
    private TmOrderService tmOrderService;

    @InjectMocks
    private TmOrderController tmOrderController;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        this.mockMvc = MockMvcBuilders.standaloneSetup(tmOrderController)
                .setControllerAdvice(new ControllerExceptionHandler()).build();


        gson = new GsonBuilder()
                .serializeNulls()
                .registerTypeAdapter(LocalDateTime.class, new LocalDateTimeAdapter())
                .registerTypeAdapter(LocalDate.class, new LocalDateAdapter())
                .create();

        AddressDto addressDto = new AddressDto();
        addressDto.setStreetName("Lidická");
        addressDto.setZipCode("60200");
        addressDto.setCity("Brno");
        addressDto.setHouseNumber("12");
        addressDto.setCountry("CZE");

        tmOrderDto = new TmOrderDto();
        tmOrderDto.setId(1L);
        tmOrderDto.setMdRate(50);
        tmOrderDto.setMdNumber(8000);
        tmOrderDto.setType(TmOrderType.INCOMING);
        tmOrderDto.setMainOrder(null);
        tmOrderDto.setValidFrom(LocalDate.now());
        tmOrderDto.setValidTo(LocalDate.now());
        tmOrderDto.setNumber("12345678");

        CompanyDto supplier = new CompanyDto();
        supplier.setId(1L);
        supplier.setCompanyId("12345678");
        supplier.setName("Wekolo");
        supplier.setAddress(addressDto);
        tmOrderDto.setSupplier(supplier);

        CompanyDto subscriber = new CompanyDto();
        subscriber.setId(2L);
        subscriber.setCompanyId("12345678");
        subscriber.setName("Wekolo");
        subscriber.setAddress(addressDto);
        tmOrderDto.setSubscriber(subscriber);

        tmOrderDto.setSubOrders(new HashSet<>());
        tmOrderDto.setCurrency("EUR");
        tmOrderDto.setDeliveries(new HashSet<>());

        TmProjectDto tmProjectDto = new TmProjectDto();
        tmProjectDto.setId(1L);
        tmProjectDto.setName("Super Project 2");
        tmProjectDto.setStatus(ProjectStatus.OPEN);
        tmOrderDto.setProject(tmProjectDto);
    }

    @Test
    public void testCreateValidParameters() throws Exception {
        Mockito.when(tmOrderService.create(Mockito.any(TmOrderDto.class))).thenReturn(tmOrderDto);

        String contentAsString = this.mockMvc.perform(MockMvcRequestBuilders
                .post(API_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(gson.toJson(tmOrderDto))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(Matchers.equalTo(tmOrderDto.getId().intValue())))
                .andExpect(jsonPath("$.supplier.id").value(Matchers.equalTo(tmOrderDto.getSupplier().getId().intValue())))
                .andExpect(jsonPath("$.subscriber.id").value(Matchers.equalTo(tmOrderDto.getSubscriber().getId().intValue())))
                .andExpect(jsonPath("$.mdNumber").value(Matchers.equalTo(tmOrderDto.getMdNumber())))
                .andExpect(jsonPath("$.mdRate").value(Matchers.equalTo(tmOrderDto.getMdRate())))
                .andExpect(jsonPath("$.currency").value(Matchers.equalTo(tmOrderDto.getCurrency())))
                .andReturn().getResponse().getContentAsString();

        System.out.println(contentAsString);

        Mockito.verify(tmOrderService).create(Mockito.any(TmOrderDto.class));
    }

    @Test
    public void testCreateInvalidParameters() throws Exception {
        tmOrderDto.setValidFrom(null);
        tmOrderDto.setValidTo(null);
        tmOrderDto.setCurrency(null);
        tmOrderDto.setMdRate(0);
        tmOrderDto.setMdNumber(0);
        tmOrderDto.setType(null);

        String validation_failed = this.mockMvc.perform(MockMvcRequestBuilders
                .post(API_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(gson.toJson(tmOrderDto))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError())
                .andExpect(jsonPath("$.invalidFields.*").value(Matchers.hasSize(6)))
                .andExpect(jsonPath("$.invalidFields.*").value(Matchers.containsInAnyOrder("type", "validFrom", "validTo", "mdRate", "mdNumber", "currency")))
                .andExpect(jsonPath("$.message").value(Matchers.equalTo("Validation failed")))
                .andReturn().getResponse().getContentAsString();

        System.out.println(validation_failed);

        Mockito.verify(tmOrderService, Mockito.times(0)).create(Mockito.any(TmOrderDto.class));
    }

    @Test
    public void testUpdateValidParameters() throws Exception {
        Mockito.when(tmOrderService.update(Mockito.any(TmOrderDto.class))).thenReturn(tmOrderDto);

        String contentAsString = this.mockMvc.perform(MockMvcRequestBuilders
                .put(API_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(gson.toJson(tmOrderDto))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(Matchers.equalTo(tmOrderDto.getId().intValue())))
                .andExpect(jsonPath("$.supplier.id").value(Matchers.equalTo(tmOrderDto.getSupplier().getId().intValue())))
                .andExpect(jsonPath("$.subscriber.id").value(Matchers.equalTo(tmOrderDto.getSubscriber().getId().intValue())))
                .andExpect(jsonPath("$.mdNumber").value(Matchers.equalTo(tmOrderDto.getMdNumber())))
                .andExpect(jsonPath("$.mdRate").value(Matchers.equalTo(tmOrderDto.getMdRate())))
                .andExpect(jsonPath("$.currency").value(Matchers.equalTo(tmOrderDto.getCurrency())))
                .andReturn().getResponse().getContentAsString();

        System.out.println(contentAsString);

        Mockito.verify(tmOrderService).update(Mockito.any(TmOrderDto.class));
    }

    @Test
    public void testUpdateInvalidParameters() throws Exception {
        tmOrderDto.setValidFrom(null);
        tmOrderDto.setValidTo(null);
        tmOrderDto.setCurrency(null);
        tmOrderDto.setMdRate(0);
        tmOrderDto.setMdNumber(0);
        tmOrderDto.setType(null);

        this.mockMvc.perform(MockMvcRequestBuilders
                .put(API_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(gson.toJson(tmOrderDto))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError())
                .andExpect(jsonPath("$.invalidFields.*").value(Matchers.hasSize(6)))
                .andExpect(jsonPath("$.invalidFields.*").value(Matchers.containsInAnyOrder("type", "validFrom", "validTo", "mdRate", "mdNumber", "currency")))
                .andExpect(jsonPath("$.message").value(Matchers.equalTo("Validation failed")));

        Mockito.verify(tmOrderService, Mockito.times(0)).create(Mockito.any(TmOrderDto.class));
    }

    @Test
    public void testDeleteValidParameter() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders
                .delete(API_URL + "/{id}", "1")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        Mockito.verify(tmOrderService).delete(Mockito.any(Long.class));
    }

    @Test
    public void testDeleteAbsent() throws Exception {
        Mockito.doThrow(new EntityNotFoundException()).when(tmOrderService).delete(11L);

        this.mockMvc.perform(MockMvcRequestBuilders
                .delete(API_URL + "/{id}", "11")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError())
                .andExpect(jsonPath("$.*").value(Matchers.hasSize(2)));

        Mockito.verify(tmOrderService).delete(Mockito.any(Long.class));
    }
}
