package cz.ita.javaee.web.controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import cz.ita.javaee.dto.AddressDto;
import cz.ita.javaee.dto.CompanyDto;
import cz.ita.javaee.dto.TmProjectDto;
import cz.ita.javaee.service.api.TmProjectService;
import cz.ita.javaee.test.SpringMvcContextTest;
import cz.ita.javaee.type.ProjectStatus;
import cz.ita.javaee.web.controller.error.ControllerExceptionHandler;
import cz.ita.javaee.web.controller.util.LocalDateTimeAdapter;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import javax.persistence.EntityNotFoundException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class TmProjectControllerTest extends SpringMvcContextTest {

    private final String API_URL = "/api/tm-project";
    private MockMvc mockMvc;
    private List<TmProjectDto> tmProjectDtos;
    private Gson gson;

    @Mock
    private TmProjectService tmProjectService;

    @InjectMocks
    private TmProjectController tmProjectController;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        this.mockMvc = MockMvcBuilders.standaloneSetup(tmProjectController)
                .setControllerAdvice(new ControllerExceptionHandler()).build();


        gson = new GsonBuilder()
                .serializeNulls()
                .registerTypeAdapter(LocalDateTime.class, new LocalDateTimeAdapter())
                .create();

        tmProjectDtos = new ArrayList<>();
        TmProjectDto project1 = new TmProjectDto();
        project1.setId(1L);
        CompanyDto customer1 = new CompanyDto();
        customer1.setId(1L);
        customer1.setCompanyId("12345678");
        customer1.setName("Best soft s.r.o.");
        AddressDto addressDto1 = new AddressDto();
        addressDto1.setStreetName("Lidická");
        addressDto1.setZipCode("60200");
        addressDto1.setCity("Brno");
        addressDto1.setHouseNumber("12");
        addressDto1.setCountry("CZE");
        customer1.setAddress(addressDto1);
        project1.setCustomer(customer1);
        project1.setName("Super Project 1");
        project1.setStatus(ProjectStatus.OPEN);
        project1.setOrders(new HashSet<>());
        project1.setNote("Pre NASA");
        project1.setCreated(LocalDateTime.now());

        TmProjectDto project2 = new TmProjectDto();
        project2.setId(2L);
        CompanyDto customer2 = new CompanyDto();
        customer2.setId(1L);
        customer2.setCompanyId("12345678");
        customer2.setName("Wekolo");
        AddressDto addressDto2 = new AddressDto();
        addressDto2.setStreetName("Farska");
        addressDto2.setZipCode("01898");
        addressDto2.setCity("Stropkov");
        addressDto2.setHouseNumber("1/a");
        addressDto2.setCountry("SVK");
        customer2.setAddress(addressDto2);
        project2.setCustomer(customer2);
        project2.setName("Super Project 2");
        project2.setStatus(ProjectStatus.OPEN);
        project2.setOrders(new HashSet<>());
        project2.setNote("Pre Cinu");
        project2.setCreated(LocalDateTime.now());

        tmProjectDtos.add(project1);
        tmProjectDtos.add(project2);
    }

    @Test
    public void testFindOneValidParameter() throws Exception {
        Mockito.when(tmProjectService.find(Mockito.anyLong())).thenReturn(tmProjectDtos.get(0));

        this.mockMvc.perform(MockMvcRequestBuilders
                .get(API_URL + "/{id}", "1")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(tmProjectDtos.get(0).getId().intValue()))
                .andExpect(jsonPath("$.name").value(tmProjectDtos.get(0).getName()))
                .andExpect(jsonPath("$.status").value(tmProjectDtos.get(0).getStatus().toString()))
                .andExpect(jsonPath("$.customer.id").value(tmProjectDtos.get(0).getCustomer().getId().intValue()))
                .andExpect(jsonPath("$.note").value(tmProjectDtos.get(0).getNote())).andReturn().getResponse().getContentAsString();

        Mockito.verify(tmProjectService).find(Mockito.any(Long.class));
    }

    @Test
    public void testFindOneAbsent() throws Exception {
        Mockito.doThrow(new EntityNotFoundException()).when(tmProjectService).find(11L);

        this.mockMvc.perform(MockMvcRequestBuilders
                .get(API_URL + "/{id}", "11")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError())
                .andExpect(jsonPath("$.*").value(Matchers.hasSize(2)));

        Mockito.verify(tmProjectService).find(Mockito.any(Long.class));
    }

    @Test
    public void testFindAllResponseList() throws Exception {
        Mockito.when(tmProjectService.findAll()).thenReturn(tmProjectDtos);

        this.mockMvc.perform(MockMvcRequestBuilders
                .get(API_URL)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").value(Matchers.hasSize(2)))
                .andExpect(jsonPath("$[1].*").value(Matchers.hasSize(7)))
                .andExpect(jsonPath("$[0].name").value(Matchers.equalTo(tmProjectDtos.get(0).getName())))
                .andExpect(jsonPath("$[1].name").value(Matchers.equalTo(tmProjectDtos.get(1).getName())))
                .andExpect(jsonPath("$[0].status").value(Matchers.equalTo(tmProjectDtos.get(0).getStatus().toString())))
                .andExpect(jsonPath("$[1].status").value(Matchers.equalTo(tmProjectDtos.get(1).getStatus().toString())))
                .andExpect(jsonPath("$[0].id").value(Matchers.equalTo(tmProjectDtos.get(0).getId().intValue())))
                .andExpect(jsonPath("$[1].id").value(Matchers.equalTo(tmProjectDtos.get(1).getId().intValue())))
                .andExpect(jsonPath("$[0].customer.id").value(Matchers.equalTo(tmProjectDtos.get(0).getCustomer().getId().intValue())))
                .andExpect(jsonPath("$[1].customer.id").value(Matchers.equalTo(tmProjectDtos.get(1).getCustomer().getId().intValue())))
                .andExpect(jsonPath("$[0].note").value(Matchers.equalTo(tmProjectDtos.get(0).getNote())))
                .andExpect(jsonPath("$[1].note").value(Matchers.equalTo(tmProjectDtos.get(1).getNote())));

        Mockito.verify(tmProjectService).findAll();
    }

    @Test
    public void testCreateValidParameters() throws Exception {
        Mockito.when(tmProjectService.create(Mockito.any(TmProjectDto.class))).thenReturn(tmProjectDtos.get(0));

        String contentAsString = this.mockMvc.perform(MockMvcRequestBuilders
                .post(API_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(gson.toJson(tmProjectDtos.get(0)))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(tmProjectDtos.get(0).getId().intValue()))
                .andExpect(jsonPath("$.name").value(tmProjectDtos.get(0).getName()))
                .andExpect(jsonPath("$.status").value(tmProjectDtos.get(0).getStatus().toString()))
                .andExpect(jsonPath("$.customer.id").value(tmProjectDtos.get(0).getCustomer().getId().intValue()))
                .andExpect(jsonPath("$.note").value(tmProjectDtos.get(0).getNote())).andReturn().getResponse().getContentAsString();

        System.out.println(contentAsString);
        Mockito.verify(tmProjectService).create(Mockito.any(TmProjectDto.class));
    }

    @Test
    public void testCreateInvalidParameters() throws Exception {
        tmProjectDtos.get(0).setId(null);
        tmProjectDtos.get(0).setCreated(null);
        tmProjectDtos.get(0).setName(null);
        tmProjectDtos.get(0).setStatus(null);

        String contentAsString = this.mockMvc.perform(MockMvcRequestBuilders
                .post(API_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(gson.toJson(tmProjectDtos.get(0)))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError())
                .andExpect(jsonPath("$.invalidFields.*").value(Matchers.hasSize(2)))
                .andExpect(jsonPath("$.invalidFields.*").value(Matchers.containsInAnyOrder("name", "status")))
                .andExpect(jsonPath("$.message").value(Matchers.equalTo("Validation failed"))).andReturn().getResponse().getContentAsString();
        System.out.println(contentAsString);
        Mockito.verify(tmProjectService, Mockito.times(0)).create(Mockito.any(TmProjectDto.class));
    }

    @Test
    public void testUpdateValidParameters() throws Exception {
        Mockito.when(tmProjectService.update(Mockito.any(TmProjectDto.class))).thenReturn(tmProjectDtos.get(0));

        this.mockMvc.perform(MockMvcRequestBuilders
                .put(API_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(gson.toJson(tmProjectDtos.get(0)))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(tmProjectDtos.get(0).getId().intValue()))
                .andExpect(jsonPath("$.name").value(tmProjectDtos.get(0).getName()))
                .andExpect(jsonPath("$.status").value(tmProjectDtos.get(0).getStatus().toString()))
                .andExpect(jsonPath("$.customer.id").value(tmProjectDtos.get(0).getCustomer().getId().intValue()))
                .andExpect(jsonPath("$.note").value(tmProjectDtos.get(0).getNote()));


        Mockito.verify(tmProjectService).update(Mockito.any(TmProjectDto.class));
    }

    @Test
    public void testUpdateInvalidParameters() throws Exception {
        tmProjectDtos.get(0).setName(null);
        tmProjectDtos.get(0).setStatus(null);

        this.mockMvc.perform(MockMvcRequestBuilders
                .put(API_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(gson.toJson(tmProjectDtos.get(0)))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError())
                .andExpect(jsonPath("$.invalidFields.*").value(Matchers.hasSize(2)))
                .andExpect(jsonPath("$.invalidFields").value(Matchers.containsInAnyOrder("name", "status")))
                .andExpect(jsonPath("$.message").value(Matchers.equalTo("Validation failed")));

        Mockito.verify(tmProjectService, Mockito.times(0)).create(Mockito.any(TmProjectDto.class));
    }

    @Test
    public void testDeleteValidParameter() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders
                .delete(API_URL + "/{id}", "1")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        Mockito.verify(tmProjectService).delete(Mockito.any(Long.class));
    }

    @Test
    public void testDeleteAbsent() throws Exception {
        Mockito.doThrow(new EntityNotFoundException()).when(tmProjectService).delete(11L);

        this.mockMvc.perform(MockMvcRequestBuilders
                .delete(API_URL + "/{id}", "11")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError())
                .andExpect(jsonPath("$.*").value(Matchers.hasSize(2)));

        Mockito.verify(tmProjectService).delete(Mockito.any(Long.class));
    }
}
