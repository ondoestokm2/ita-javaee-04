package cz.ita.javaee.web.controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import cz.ita.javaee.dto.AddressDto;
import cz.ita.javaee.dto.EmployeeDto;
import cz.ita.javaee.service.api.EmployeeService;
import cz.ita.javaee.test.SpringMvcContextTest;
import cz.ita.javaee.web.controller.error.ControllerExceptionHandler;
import cz.ita.javaee.web.controller.util.LocalDateTimeAdapter;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import javax.persistence.EntityNotFoundException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class EmployeeControllerTest extends SpringMvcContextTest {

    private final String API_URL = "/api/employee";
    private MockMvc mockMvc;
    private List<EmployeeDto> employees;
    private Gson gson;

    @Mock
    private EmployeeService employeeService;

    @InjectMocks
    private EmployeeController employeeController;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        this.mockMvc = MockMvcBuilders.standaloneSetup(employeeController)
                .setControllerAdvice(new ControllerExceptionHandler()).build();


        gson = new GsonBuilder()
                .serializeNulls()
                .registerTypeAdapter(LocalDateTime.class, new LocalDateTimeAdapter())
                .create();

        employees = new ArrayList<>();
        EmployeeDto employeeDto1 = new EmployeeDto();
        employeeDto1.setId(1L);
        employeeDto1.setFirstName("Fero");
        employeeDto1.setSurname("Mrkvicka");
        employeeDto1.setActive(true);
        employeeDto1.setNote("Je to borec");
        employeeDto1.setCreated(LocalDateTime.now());
        AddressDto addressDto1 = new AddressDto();
        addressDto1.setStreetName("Lidická");
        addressDto1.setZipCode("60200");
        addressDto1.setCity("Brno");
        addressDto1.setHouseNumber("12");
        addressDto1.setCountry("CZE");
        employeeDto1.setAddress(addressDto1);

        EmployeeDto employeeDto2 = new EmployeeDto();
        employeeDto2.setId(2L);
        employeeDto2.setFirstName("Antonin");
        employeeDto2.setSurname("Novak");
        employeeDto2.setActive(true);
        employeeDto2.setNote("Manazeris");
        employeeDto2.setCreated(LocalDateTime.now());
        AddressDto addressDto2 = new AddressDto();
        addressDto2.setStreetName("Farska");
        addressDto2.setZipCode("01898");
        addressDto2.setCity("Stropkov");
        addressDto2.setHouseNumber("1/a");
        addressDto2.setCountry("SVK");
        employeeDto2.setAddress(addressDto2);

        employees.add(employeeDto1);
        employees.add(employeeDto2);
    }

    @Test
    public void testFindAllResponseList() throws Exception {
        Mockito.when(employeeService.findAll()).thenReturn(employees);

        this.mockMvc.perform(MockMvcRequestBuilders
                .get(API_URL)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").value(Matchers.hasSize(2)))
                .andExpect(jsonPath("$[1].*").value(Matchers.hasSize(7)))
                .andExpect(jsonPath("$[0].firstName").value(Matchers.equalTo(employees.get(0).getFirstName())))
                .andExpect(jsonPath("$[1].firstName").value(Matchers.equalTo(employees.get(1).getFirstName())))
                .andExpect(jsonPath("$[0].surname").value(Matchers.equalTo(employees.get(0).getSurname())))
                .andExpect(jsonPath("$[1].surname").value(Matchers.equalTo(employees.get(1).getSurname())))
                .andExpect(jsonPath("$[0].id").value(Matchers.equalTo(employees.get(0).getId().intValue())))
                .andExpect(jsonPath("$[1].id").value(Matchers.equalTo(employees.get(1).getId().intValue())))
                .andExpect(jsonPath("$[0].address.city").value(Matchers.equalTo(employees.get(0).getAddress().getCity())))
                .andExpect(jsonPath("$[1].address.city").value(Matchers.equalTo(employees.get(1).getAddress().getCity())))
                .andExpect(jsonPath("$[0].address.country").value(Matchers.equalTo(employees.get(0).getAddress().getCountry())))
                .andExpect(jsonPath("$[1].address.country").value(Matchers.equalTo(employees.get(1).getAddress().getCountry())))
                .andExpect(jsonPath("$[0].address.houseNumber").value(Matchers.equalTo(employees.get(0).getAddress().getHouseNumber())))
                .andExpect(jsonPath("$[1].address.houseNumber").value(Matchers.equalTo(employees.get(1).getAddress().getHouseNumber())))
                .andExpect(jsonPath("$[0].address.streetName").value(Matchers.equalTo(employees.get(0).getAddress().getStreetName())))
                .andExpect(jsonPath("$[1].address.streetName").value(Matchers.equalTo(employees.get(1).getAddress().getStreetName())))
                .andExpect(jsonPath("$[0].address.zipCode").value(Matchers.equalTo(employees.get(0).getAddress().getZipCode())))
                .andExpect(jsonPath("$[1].address.zipCode").value(Matchers.equalTo(employees.get(1).getAddress().getZipCode())))
                .andExpect(jsonPath("$[0].active").value(Matchers.equalTo(employees.get(0).isActive())))
                .andExpect(jsonPath("$[1].active").value(Matchers.equalTo(employees.get(1).isActive())));

        Mockito.verify(employeeService).findAll();
    }

    @Test
    public void testCreateValidParameters() throws Exception {
        Mockito.when(employeeService.create(Mockito.any(EmployeeDto.class))).thenReturn(employees.get(0));

        this.mockMvc.perform(MockMvcRequestBuilders
                .post(API_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(gson.toJson(employees.get(0)))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.firstName").value(Matchers.equalTo(employees.get(0).getFirstName())))
                .andExpect(jsonPath("$.surname").value(Matchers.equalTo(employees.get(0).getSurname())))
                .andExpect(jsonPath("$.address.streetName").value(Matchers.equalTo(employees.get(0).getAddress().getStreetName())))
                .andExpect(jsonPath("$.address.houseNumber").value(Matchers.equalTo(employees.get(0).getAddress().getHouseNumber())))
                .andExpect(jsonPath("$.address.city").value(Matchers.equalTo(employees.get(0).getAddress().getCity())))
                .andExpect(jsonPath("$.address.country").value(Matchers.equalTo(employees.get(0).getAddress().getCountry())))
                .andExpect(jsonPath("$.address.zipCode").value(Matchers.equalTo(employees.get(0).getAddress().getZipCode())))
                .andExpect(jsonPath("$.active").value(Matchers.equalTo(employees.get(0).isActive())))
                .andExpect(jsonPath("$.note").value(Matchers.equalTo(employees.get(0).getNote())))
                .andExpect(jsonPath("$.id").value(Matchers.is(employees.get(0).getId().intValue())));

        Mockito.verify(employeeService).create(Mockito.any(EmployeeDto.class));
    }

    @Test
    public void testCreateInvalidParameters() throws Exception {
        employees.get(0).setId(null);
        employees.get(0).setCreated(null);
        employees.get(0).setFirstName(null);
        employees.get(0).setSurname(null);

        this.mockMvc.perform(MockMvcRequestBuilders
                .post(API_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(gson.toJson(employees.get(0)))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError())
                .andExpect(jsonPath("$.invalidFields.*").value(Matchers.hasSize(2)))
                .andExpect(jsonPath("$.invalidFields.*").value(Matchers.containsInAnyOrder("firstName", "surname")))
                .andExpect(jsonPath("$.message").value(Matchers.equalTo("Validation failed")));

        Mockito.verify(employeeService, Mockito.times(0)).create(Mockito.any(EmployeeDto.class));
    }

    @Test
    public void testUpdateValidParameters() throws Exception {
        Mockito.when(employeeService.update(Mockito.any(EmployeeDto.class))).thenReturn(employees.get(0));

        this.mockMvc.perform(MockMvcRequestBuilders
                .put(API_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(gson.toJson(employees.get(0)))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(employees.get(0).getId().intValue()))
                .andExpect(jsonPath("$.firstName").value(employees.get(0).getFirstName()))
                .andExpect(jsonPath("$.surname").value(employees.get(0).getSurname()))
                .andExpect(jsonPath("$.active").value(employees.get(0).isActive()))
                .andExpect(jsonPath("$.note").value(employees.get(0).getNote()))
                .andExpect(jsonPath("$.address.streetName").value(employees.get(0).getAddress().getStreetName()))
                .andExpect(jsonPath("$.address.zipCode").value(employees.get(0).getAddress().getZipCode()))
                .andExpect(jsonPath("$.address.city").value(employees.get(0).getAddress().getCity()))
                .andExpect(jsonPath("$.address.houseNumber").value(employees.get(0).getAddress().getHouseNumber()))
                .andExpect(jsonPath("$.address.country").value(employees.get(0).getAddress().getCountry()));

        Mockito.verify(employeeService).update(Mockito.any(EmployeeDto.class));
    }

    @Test
    public void testUpdateInvalidParameters() throws Exception {
        employees.get(0).setFirstName(null);
        employees.get(0).setSurname(null);

        this.mockMvc.perform(MockMvcRequestBuilders
                .put(API_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(gson.toJson(employees.get(0)))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError())
                .andExpect(jsonPath("$.invalidFields.*").value(Matchers.hasSize(2)))
                .andExpect(jsonPath("$.invalidFields").value(Matchers.containsInAnyOrder("firstName", "surname")))
                .andExpect(jsonPath("$.message").value(Matchers.equalTo("Validation failed")));

        Mockito.verify(employeeService, Mockito.times(0)).create(Mockito.any(EmployeeDto.class));
    }

    @Test
    public void testDeleteValidParameter() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders
                .delete(API_URL + "/{id}", "1")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        Mockito.verify(employeeService).delete(Mockito.any(Long.class));
    }

    @Test
    public void testDeleteAbsent() throws Exception {
        Mockito.doThrow(new EntityNotFoundException()).when(employeeService).delete(11L);

        this.mockMvc.perform(MockMvcRequestBuilders
                .delete(API_URL + "/{id}", "11")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError())
                .andExpect(jsonPath("$.*").value(Matchers.hasSize(2)));

        Mockito.verify(employeeService).delete(Mockito.any(Long.class));
    }
}
