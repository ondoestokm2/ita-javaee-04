package cz.ita.javaee.web.controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import cz.ita.javaee.dto.AddressDto;
import cz.ita.javaee.dto.CompanyDto;
import cz.ita.javaee.service.api.CompanyService;
import cz.ita.javaee.test.SpringMvcContextTest;
import cz.ita.javaee.web.controller.error.ControllerExceptionHandler;
import cz.ita.javaee.web.controller.util.LocalDateTimeAdapter;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import javax.persistence.EntityNotFoundException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class CompanyControllerTest extends SpringMvcContextTest {

    private final String API_URL = "/api/company";
    private MockMvc mockMvc;
    private List<CompanyDto> companies;
    private Gson gson;

    @Mock
    private CompanyService companyService;

    @InjectMocks
    private CompanyController companyController;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        this.mockMvc = MockMvcBuilders.standaloneSetup(companyController)
                .setControllerAdvice(new ControllerExceptionHandler()).build();


        gson = new GsonBuilder()
                .serializeNulls()
                .registerTypeAdapter(LocalDateTime.class, new LocalDateTimeAdapter())
                .create();

        companies = new ArrayList<>();
        CompanyDto companyDto1 = new CompanyDto();
        companyDto1.setId(1L);
        companyDto1.setName("Best soft s.r.o.");
        companyDto1.setCompanyId("11223344");
        companyDto1.setVatId("CY11223344");
        companyDto1.setActive(true);
        companyDto1.setOwnershipped(true);
        companyDto1.setNote("Hlavní firma.");
        companyDto1.setCreated(LocalDateTime.now());
        AddressDto addressDto1 = new AddressDto();
        addressDto1.setStreetName("Lidická");
        addressDto1.setZipCode("60200");
        addressDto1.setCity("Brno");
        addressDto1.setHouseNumber("12");
        addressDto1.setCountry("CZE");
        companyDto1.setAddress(addressDto1);

        CompanyDto companyDto2 = new CompanyDto();
        companyDto2.setId(2L);
        companyDto2.setName("Wekolo");
        companyDto2.setCompanyId("11112222");
        companyDto2.setVatId("SR11112222");
        companyDto2.setActive(true);
        companyDto2.setOwnershipped(false);
        companyDto2.setNote("Hlavní firma 2.");
        companyDto2.setCreated(LocalDateTime.now());
        AddressDto addressDto2 = new AddressDto();
        addressDto2.setStreetName("Farska");
        addressDto2.setZipCode("01898");
        addressDto2.setCity("Stropkov");
        addressDto2.setHouseNumber("1/a");
        addressDto2.setCountry("SVK");
        companyDto2.setAddress(addressDto2);

        companies.add(companyDto1);
        companies.add(companyDto2);
    }

    @Test
    public void testFindAllResponseList() throws Exception {
        Mockito.when(companyService.findAll()).thenReturn(companies);

        this.mockMvc.perform(MockMvcRequestBuilders
                .get(API_URL)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").value(Matchers.hasSize(2)))
                .andExpect(jsonPath("$[1].*").value(Matchers.hasSize(9)))
                .andExpect(jsonPath("$[0].name").value(Matchers.equalTo(companies.get(0).getName())))
                .andExpect(jsonPath("$[1].name").value(Matchers.equalTo(companies.get(1).getName())))
                .andExpect(jsonPath("$[0].companyId").value(Matchers.equalTo(companies.get(0).getCompanyId())))
                .andExpect(jsonPath("$[1].companyId").value(Matchers.equalTo(companies.get(1).getCompanyId())))
                .andExpect(jsonPath("$[0].id").value(Matchers.equalTo(companies.get(0).getId().intValue())))
                .andExpect(jsonPath("$[1].id").value(Matchers.equalTo(companies.get(1).getId().intValue())))
                .andExpect(jsonPath("$[0].vatId").value(Matchers.equalTo(companies.get(0).getVatId())))
                .andExpect(jsonPath("$[1].vatId").value(Matchers.equalTo(companies.get(1).getVatId())))
                .andExpect(jsonPath("$[0].address.city").value(Matchers.equalTo(companies.get(0).getAddress().getCity())))
                .andExpect(jsonPath("$[1].address.city").value(Matchers.equalTo(companies.get(1).getAddress().getCity())))
                .andExpect(jsonPath("$[0].address.country").value(Matchers.equalTo(companies.get(0).getAddress().getCountry())))
                .andExpect(jsonPath("$[1].address.country").value(Matchers.equalTo(companies.get(1).getAddress().getCountry())))
                .andExpect(jsonPath("$[0].address.houseNumber").value(Matchers.equalTo(companies.get(0).getAddress().getHouseNumber())))
                .andExpect(jsonPath("$[1].address.houseNumber").value(Matchers.equalTo(companies.get(1).getAddress().getHouseNumber())))
                .andExpect(jsonPath("$[0].address.streetName").value(Matchers.equalTo(companies.get(0).getAddress().getStreetName())))
                .andExpect(jsonPath("$[1].address.streetName").value(Matchers.equalTo(companies.get(1).getAddress().getStreetName())))
                .andExpect(jsonPath("$[0].address.zipCode").value(Matchers.equalTo(companies.get(0).getAddress().getZipCode())))
                .andExpect(jsonPath("$[1].address.zipCode").value(Matchers.equalTo(companies.get(1).getAddress().getZipCode())))
                .andExpect(jsonPath("$[0].ownershipped").value(Matchers.equalTo(companies.get(0).isOwnershipped())))
                .andExpect(jsonPath("$[1].ownershipped").value(Matchers.equalTo(companies.get(1).isOwnershipped())));

        Mockito.verify(companyService).findAll();
    }

    @Test
    public void testCreateValidParameters() throws Exception {
        Mockito.when(companyService.create(Mockito.any(CompanyDto.class))).thenReturn(companies.get(0));

        this.mockMvc.perform(MockMvcRequestBuilders
                .post(API_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(gson.toJson(companies.get(0)))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value(Matchers.equalTo(companies.get(0).getName())))
                .andExpect(jsonPath("$.companyId").value(Matchers.equalTo(companies.get(0).getCompanyId())))
                .andExpect(jsonPath("$.vatId").value(Matchers.equalTo(companies.get(0).getVatId())))
                .andExpect(jsonPath("$.address.streetName").value(Matchers.equalTo(companies.get(0).getAddress().getStreetName())))
                .andExpect(jsonPath("$.address.houseNumber").value(Matchers.equalTo(companies.get(0).getAddress().getHouseNumber())))
                .andExpect(jsonPath("$.address.city").value(Matchers.equalTo(companies.get(0).getAddress().getCity())))
                .andExpect(jsonPath("$.address.country").value(Matchers.equalTo(companies.get(0).getAddress().getCountry())))
                .andExpect(jsonPath("$.address.zipCode").value(Matchers.equalTo(companies.get(0).getAddress().getZipCode())))
                .andExpect(jsonPath("$.ownershipped").value(Matchers.equalTo(companies.get(0).isOwnershipped())))
                .andExpect(jsonPath("$.active").value(Matchers.equalTo(companies.get(0).isActive())))
                .andExpect(jsonPath("$.note").value(Matchers.equalTo(companies.get(0).getNote())))
                .andExpect(jsonPath("$.id").value(Matchers.is(companies.get(0).getId().intValue())));

        Mockito.verify(companyService).create(Mockito.any(CompanyDto.class));
    }

    @Test
    public void testCreateInvalidParameters() throws Exception {
        companies.get(0).setId(null);
        companies.get(0).setCreated(null);
        companies.get(0).setName(null);
        companies.get(0).setCompanyId("1234");

        this.mockMvc.perform(MockMvcRequestBuilders
                .post(API_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(gson.toJson(companies.get(0)))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError())
                .andExpect(jsonPath("$.invalidFields.*").value(Matchers.hasSize(2)))
                .andExpect(jsonPath("$.invalidFields.*").value(Matchers.containsInAnyOrder("name", "companyId")))
                .andExpect(jsonPath("$.message").value(Matchers.equalTo("Validation failed")));

        Mockito.verify(companyService, Mockito.times(0)).create(Mockito.any(CompanyDto.class));
    }

    @Test
    public void testUpdateValidParameters() throws Exception {
        Mockito.when(companyService.update(Mockito.any(CompanyDto.class))).thenReturn(companies.get(0));

        this.mockMvc.perform(MockMvcRequestBuilders
                .put(API_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(gson.toJson(companies.get(0)))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(companies.get(0).getId().intValue()))
                .andExpect(jsonPath("$.name").value(companies.get(0).getName()))
                .andExpect(jsonPath("$.companyId").value(companies.get(0).getCompanyId()))
                .andExpect(jsonPath("$.vatId").value(companies.get(0).getVatId()))
                .andExpect(jsonPath("$.active").value(companies.get(0).isActive()))
                .andExpect(jsonPath("$.ownershipped").value(companies.get(0).isOwnershipped()))
                .andExpect(jsonPath("$.note").value(companies.get(0).getNote()))
                .andExpect(jsonPath("$.address.streetName").value(companies.get(0).getAddress().getStreetName()))
                .andExpect(jsonPath("$.address.zipCode").value(companies.get(0).getAddress().getZipCode()))
                .andExpect(jsonPath("$.address.city").value(companies.get(0).getAddress().getCity()))
                .andExpect(jsonPath("$.address.houseNumber").value(companies.get(0).getAddress().getHouseNumber()))
                .andExpect(jsonPath("$.address.country").value(companies.get(0).getAddress().getCountry()));

        Mockito.verify(companyService).update(Mockito.any(CompanyDto.class));
    }

    @Test
    public void testUpdateInvalidParameters() throws Exception {
        companies.get(0).setName(null);
        companies.get(0).setCompanyId("1234");

        this.mockMvc.perform(MockMvcRequestBuilders
                .put(API_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(gson.toJson(companies.get(0)))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError())
                .andExpect(jsonPath("$.invalidFields.*").value(Matchers.hasSize(2)))
                .andExpect(jsonPath("$.invalidFields").value(Matchers.containsInAnyOrder("name", "companyId")))
                .andExpect(jsonPath("$.message").value(Matchers.equalTo("Validation failed")));

        Mockito.verify(companyService, Mockito.times(0)).create(Mockito.any(CompanyDto.class));
    }

    @Test
    public void testDeleteValidParameter() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders
                .delete(API_URL + "/{id}", "1")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        Mockito.verify(companyService).delete(Mockito.any(Long.class));
    }

    @Test
    public void testDeleteAbsent() throws Exception {
        Mockito.doThrow(new EntityNotFoundException()).when(companyService).delete(11L);

        this.mockMvc.perform(MockMvcRequestBuilders
                .delete(API_URL + "/{id}", "11")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError())
                .andExpect(jsonPath("$.*").value(Matchers.hasSize(2)));

        Mockito.verify(companyService).delete(Mockito.any(Long.class));
    }
}
