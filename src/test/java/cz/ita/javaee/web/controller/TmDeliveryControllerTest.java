package cz.ita.javaee.web.controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import cz.ita.javaee.dto.TmDeliveryDto;
import cz.ita.javaee.dto.TmOrderDto;
import cz.ita.javaee.service.api.TmDeliveryService;
import cz.ita.javaee.test.SpringMvcContextTest;
import cz.ita.javaee.type.TmOrderType;
import cz.ita.javaee.web.controller.error.ControllerExceptionHandler;
import cz.ita.javaee.web.controller.util.LocalDateAdapter;
import cz.ita.javaee.web.controller.util.LocalDateTimeAdapter;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import javax.persistence.EntityNotFoundException;
import java.time.LocalDate;
import java.time.LocalDateTime;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class TmDeliveryControllerTest extends SpringMvcContextTest {

    private final String API_URL = "/api/tm-delivery";
    private MockMvc mockMvc;
    private TmDeliveryDto tmDeliveryDto;
    private Gson gson;

    @Mock
    private TmDeliveryService tmDeliveryService;

    @InjectMocks
    private TmDeliveryController tmDeliveryController;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        this.mockMvc = MockMvcBuilders.standaloneSetup(tmDeliveryController)
                .setControllerAdvice(new ControllerExceptionHandler()).build();


        gson = new GsonBuilder()
                .serializeNulls()
                .registerTypeAdapter(LocalDateTime.class, new LocalDateTimeAdapter())
                .registerTypeAdapter(LocalDate.class, new LocalDateAdapter())
                .create();

        tmDeliveryDto = new TmDeliveryDto();
        tmDeliveryDto.setId(1L);
        tmDeliveryDto.setCreated(LocalDateTime.now());
        tmDeliveryDto.setMdNumber(1500);
        tmDeliveryDto.setPeriod(LocalDate.now());
        TmOrderDto orderDto = new TmOrderDto();
        orderDto.setId(55L);
        orderDto.setMdNumber(5000);
        orderDto.setMdRate(500);
        orderDto.setType(TmOrderType.INCOMING);
        orderDto.setNumber("20180001");
        orderDto.setCurrency("EUR");
        orderDto.setValidFrom(LocalDate.now());
        orderDto.setValidTo(LocalDate.now());
        tmDeliveryDto.setOrder(orderDto);
    }

    @Test
    public void testCreateValidParameters() throws Exception {
        tmDeliveryDto.setId(null);
        tmDeliveryDto.setCreated(null);

        Mockito.when(tmDeliveryService.create(Mockito.any(TmDeliveryDto.class))).thenReturn(tmDeliveryDto);

        this.mockMvc.perform(MockMvcRequestBuilders
                .post(API_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(gson.toJson(tmDeliveryDto))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(Matchers.equalTo(tmDeliveryDto.getId())))
                .andExpect(jsonPath("$.order.mdNumber").value(Matchers.equalTo(tmDeliveryDto.getOrder().getMdNumber())))
                .andExpect(jsonPath("$.order.mdRate").value(Matchers.equalTo(tmDeliveryDto.getOrder().getMdRate())))
                .andExpect(jsonPath("$.order.type").value(Matchers.equalTo(tmDeliveryDto.getOrder().getType().toString())))
                .andExpect(jsonPath("$.order.currency").value(Matchers.equalTo(tmDeliveryDto.getOrder().getCurrency())));

        Mockito.verify(tmDeliveryService).create(Mockito.any(TmDeliveryDto.class));
    }

    @Test
    public void testCreateInvalidParameters() throws Exception {
        tmDeliveryDto.setMdNumber(0);

        String contentAsString = this.mockMvc.perform(MockMvcRequestBuilders
                .post(API_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(gson.toJson(tmDeliveryDto))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError())
                .andExpect(jsonPath("$.invalidFields.*").value(Matchers.hasSize(1)))
                .andExpect(jsonPath("$.invalidFields.*").value(Matchers.containsInAnyOrder("mdNumber")))
                .andExpect(jsonPath("$.message").value(Matchers.equalTo("Validation failed"))).andReturn().getResponse().getContentAsString();

        System.out.println(contentAsString);
        //Mockito.verify(tmDeliveryService, Mockito.times(0)).create(Mockito.any(TmDeliveryDto.class));
    }

    @Test
    public void testUpdateValidParameters() throws Exception {
        Mockito.when(tmDeliveryService.update(Mockito.any(TmDeliveryDto.class))).thenReturn(tmDeliveryDto);

        this.mockMvc.perform(MockMvcRequestBuilders
                .put(API_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(gson.toJson(tmDeliveryDto))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(tmDeliveryDto.getId().intValue()))
                .andExpect(jsonPath("$.mdNumber").value(tmDeliveryDto.getMdNumber()))
                .andExpect(jsonPath("$.order.id").value(tmDeliveryDto.getOrder().getId()));

        Mockito.verify(tmDeliveryService).update(Mockito.any(TmDeliveryDto.class));
    }

    @Test
    public void testUpdateInvalidParameters() throws Exception {
        tmDeliveryDto.setMdNumber(0);

        this.mockMvc.perform(MockMvcRequestBuilders
                .put(API_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(gson.toJson(tmDeliveryDto))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError())
                .andExpect(jsonPath("$.invalidFields.*").value(Matchers.hasSize(1)))
                .andExpect(jsonPath("$.invalidFields").value(Matchers.containsInAnyOrder("mdNumber")))
                .andExpect(jsonPath("$.message").value(Matchers.equalTo("Validation failed")));

        Mockito.verify(tmDeliveryService, Mockito.times(0)).create(Mockito.any(TmDeliveryDto.class));
    }

    @Test
    public void testDeleteValidParameter() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders
                .delete(API_URL + "/{id}", "1")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        Mockito.verify(tmDeliveryService).delete(Mockito.any(Long.class));
    }

    @Test
    public void testDeleteAbsent() throws Exception {
        Mockito.doThrow(new EntityNotFoundException()).when(tmDeliveryService).delete(11L);

        this.mockMvc.perform(MockMvcRequestBuilders
                .delete(API_URL + "/{id}", "11")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError())
                .andExpect(jsonPath("$.*").value(Matchers.hasSize(2)));

        Mockito.verify(tmDeliveryService).delete(Mockito.any(Long.class));
    }
}
