package cz.ita.javaee.matcher;

import cz.ita.javaee.model.EmployeeEntity;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

import java.time.LocalDateTime;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsAnything.anything;

public class EmployeeMatcher extends TypeSafeMatcher<EmployeeEntity> {

    private Matcher<? super Long> id = is(anything());
    private Matcher<? super LocalDateTime> created = is(anything());
    private Matcher<? super Boolean> active = is(anything());
    private Matcher<? super String> note = is(anything());
    private Matcher<? super String> firstName = is(anything());
    private Matcher<? super String> surname = is(anything());
    private Matcher<? super String> addressStreetName = is(anything());
    private Matcher<? super String> addressHouseNumber = is(anything());
    private Matcher<? super String> addressCity = is(anything());
    private Matcher<? super String> addressCountry = is(anything());
    private Matcher<? super String> addressZipCode = is(anything());

    public EmployeeMatcher(EmployeeEntity entity) {
        id = is(entity.getId());
        created = is(entity.getCreated());
        active = is(entity.isActive());
        note = is(entity.getNote());
        firstName = is(entity.getFirstName());
        surname = is(entity.getSurname());
        addressStreetName = is(entity.getAddress().getStreetName());
        addressHouseNumber = is(entity.getAddress().getHouseNumber());
        addressCity = is(entity.getAddress().getCity());
        addressCountry = is(entity.getAddress().getCountry());
        addressZipCode = is(entity.getAddress().getZipCode());
    }


    @Override
    protected boolean matchesSafely(EmployeeEntity item) {
        boolean match = true;
        match &= id.matches(item.getId());
        match &= created.matches(item.getCreated());
        match &= active.matches(item.isActive());
        match &= note.matches(item.getNote());
        match &= firstName.matches(item.getFirstName());
        match &= surname.matches(item.getSurname());
        match &= addressStreetName.matches((item.getAddress().getStreetName()));
        match &= addressHouseNumber.matches((item.getAddress().getHouseNumber()));
        match &= addressCity.matches((item.getAddress().getCity()));
        match &= addressCountry.matches((item.getAddress().getCountry()));
        match &= addressZipCode.matches((item.getAddress().getZipCode()));

        return match;
    }

    @Override
    public void describeTo(Description description) {
        description.appendText("id: ").appendDescriptionOf(id).appendText("\n");
        description.appendText("created: ").appendDescriptionOf(created).appendText("\n");
        description.appendText("active: ").appendDescriptionOf(active).appendText("\n");
        description.appendText("note: ").appendDescriptionOf(note).appendText("\n");
        description.appendText("firstName: ").appendDescriptionOf(firstName).appendText("\n");
        description.appendText("surname: ").appendDescriptionOf(surname).appendText("\n");
        description.appendText("addressStreetName: ").appendDescriptionOf(addressStreetName).appendText("\n");
        description.appendText("addressHouseNumber: ").appendDescriptionOf(addressHouseNumber).appendText("\n");
        description.appendText("addressCity: ").appendDescriptionOf(addressCity).appendText("\n");
        description.appendText("addressCountry: ").appendDescriptionOf(addressCountry).appendText("\n");
        description.appendText("addressZipCode: ").appendDescriptionOf(addressZipCode).appendText("\n");
    }
}
