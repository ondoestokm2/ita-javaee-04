package cz.ita.javaee.matcher;

import cz.ita.javaee.model.CompanyEntity;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

import java.time.LocalDateTime;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsAnything.anything;

public class CompanyMatcher extends TypeSafeMatcher<CompanyEntity> {

    private Matcher<? super Long> id = is(anything());
    private Matcher<? super LocalDateTime> created = is(anything());
    private Matcher<? super Boolean> active = is(anything());
    private Matcher<? super String> note = is(anything());
    private Matcher<? super String> name = is(anything());
    private Matcher<? super String> companyId = is(anything());
    private Matcher<? super String> vatId = is(anything());
    private Matcher<? super Boolean> ownershipped = is(anything());
    private Matcher<? super String> addressStreetName = is(anything());
    private Matcher<? super String> addressHouseNumber = is(anything());
    private Matcher<? super String> addressCity = is(anything());
    private Matcher<? super String> addressCountry = is(anything());
    private Matcher<? super String> addressZipCode = is(anything());

    public CompanyMatcher(CompanyEntity entity) {
        id = is(entity.getId());
        created = is(entity.getCreated());
        active = is(entity.isActive());
        note = is(entity.getNote());
        name = is(entity.getName());
        companyId = is(entity.getCompanyId());
        vatId = is(entity.getVatId());
        ownershipped = is(entity.isOwnershipped());
        addressStreetName = is(entity.getAddress().getStreetName());
        addressHouseNumber = is(entity.getAddress().getHouseNumber());
        addressCity = is(entity.getAddress().getCity());
        addressCountry = is(entity.getAddress().getCountry());
        addressZipCode = is(entity.getAddress().getZipCode());
    }

    @Override
    protected boolean matchesSafely(CompanyEntity item) {
        boolean match = true;
        match &= id.matches(item.getId());
        match &= created.matches(item.getCreated());
        match &= active.matches(item.isActive());
        match &= note.matches(item.getNote());
        match &= name.matches(item.getName());
        match &= companyId.matches(item.getCompanyId());
        match &= vatId.matches(item.getVatId());
        match &= ownershipped.matches(item.isOwnershipped());
        match &= addressStreetName.matches((item.getAddress().getStreetName()));
        match &= addressHouseNumber.matches((item.getAddress().getHouseNumber()));
        match &= addressCity.matches((item.getAddress().getCity()));
        match &= addressCountry.matches((item.getAddress().getCountry()));
        match &= addressZipCode.matches((item.getAddress().getZipCode()));
        return match;
    }

    @Override
    public void describeTo(Description description) {
        description.appendText("id: ").appendDescriptionOf(id).appendText("\n");
        description.appendText("created: ").appendDescriptionOf(created).appendText("\n");
        description.appendText("note: ").appendDescriptionOf(note).appendText("\n");
        description.appendText("name: ").appendDescriptionOf(name).appendText("\n");
        description.appendText("companyId: ").appendDescriptionOf(companyId).appendText("\n");
        description.appendText("vatId: ").appendDescriptionOf(vatId).appendText("\n");
        description.appendText("ownershipped: ").appendDescriptionOf(ownershipped).appendText("\n");
        description.appendText("addressStreetName: ").appendDescriptionOf(addressStreetName).appendText("\n");
        description.appendText("addressHouseNumber: ").appendDescriptionOf(addressHouseNumber).appendText("\n");
        description.appendText("addressCity: ").appendDescriptionOf(addressCity).appendText("\n");
        description.appendText("addressCountry: ").appendDescriptionOf(addressCountry).appendText("\n");
        description.appendText("addressZipCode: ").appendDescriptionOf(addressZipCode).appendText("\n");
    }
}
