package cz.ita.javaee.matcher;

import cz.ita.javaee.model.TmProjectEntity;
import cz.ita.javaee.type.ProjectStatus;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

import java.time.LocalDateTime;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsAnything.anything;

public class TmProjectMatcher extends TypeSafeMatcher<TmProjectEntity> {

    private Matcher<? super Long> id = is(anything());
    private Matcher<? super LocalDateTime> created = is(anything());
    private Matcher<? super String> name = is(anything());
    private Matcher<? super Long> customerId = is(anything());
    private Matcher<? super ProjectStatus> projectStatus = is(anything());
    private Matcher<? super String> note = is(anything());

    public TmProjectMatcher(TmProjectEntity entity) {
        id = is(entity.getId());
        created = is(entity.getCreated());
        name = is(entity.getName());
        customerId = is(entity.getCustomer().getId());
        projectStatus = is(entity.getStatus());
        note = is(entity.getNote());
    }

    @Override
    protected boolean matchesSafely(TmProjectEntity item) {
        boolean match = true;
        match &= id.matches(item.getId());
        match &= created.matches(item.getCreated());
        match &= name.matches(item.getName());
        match &= customerId.matches(item.getCustomer().getId());
        match &= projectStatus.matches(item.getStatus());
        match &= note.matches(item.getNote());

        return match;
    }

    @Override
    public void describeTo(Description description) {
        description.appendText("id: ").appendDescriptionOf(id).appendText("\n");
        description.appendText("created: ").appendDescriptionOf(created).appendText("\n");
        description.appendText("name: ").appendDescriptionOf(name).appendText("\n");
        description.appendText("customerId: ").appendDescriptionOf(customerId).appendText("\n");
        description.appendText("projectStatus: ").appendDescriptionOf(projectStatus).appendText("\n");
        description.appendText("note: ").appendDescriptionOf(note).appendText("\n");
    }
}
