-- SUBJECT INSERTS FOR COMAPNY AND EMPLOYEE (2)
INSERT INTO subject (created, active, note) VALUES ('2013-06-01', TRUE, 'Spolahivy klient');
INSERT INTO subject (created, active, note) VALUES ('2017-06-01', TRUE, 'Platia faktury vcas');
INSERT INTO subject (created, active, note) VALUES ('2015-06-01', TRUE, 'Špecialista na Spring a Angular');
INSERT INTO subject (created, active, note) VALUES ('2017-06-01', TRUE, 'Špecialista na .NET a React');

-- COMPANIES INSERTS (2)
INSERT INTO company (id, address_city, address_country, address_house_number, address_street_name, address_zip_code, company_id, name, ownershipped, vat_id) VALUES (1, 'Praha', 'CZE', '25', 'Gorkého', '04514', '87654321', 'Microsoft a.s.', true, NULL);
INSERT INTO company (id, address_city, address_country, address_house_number, address_street_name, address_zip_code, company_id, name, ownershipped, vat_id) VALUES (2, 'Brno', 'CZE', '4', 'Bublifuková', '11234', '12345678', 'SuperSoft a.s.', false, NULL);
-- EMPLOYEES INSERTS (2)

INSERT INTO employee (id, address_city, address_country, address_house_number, address_street_name, address_zip_code, first_name, surname) VALUES (3, 'Brno', 'CZE', '11', 'Jakubcová', '11234', 'Michal', 'Hubička');
INSERT INTO employee (id, address_city, address_country, address_house_number, address_street_name, address_zip_code, first_name, surname) VALUES (4, 'Praha', 'CZE', '2', 'Patakyho', '04514', 'Peter', 'Husovský');

-- PROJECTS INSERTS (2)
INSERT INTO project (created, name, note, project_status, customer_id) VALUES ('2017-09-13', 'Lumis 2000', 'Poznamka pre Lumis 1', 'OPEN', 2);
INSERT INTO project_tm (id) VALUES (1);
INSERT INTO project (created, name, note, project_status, customer_id) VALUES ('2016-09-13', 'Lumis 1000', 'Poznamka pre Lumis 2', 'OPEN', 2);
INSERT INTO project_tm (id) VALUES (2);
INSERT INTO project (created, name, note, project_status, customer_id) VALUES ('2016-09-13', 'Asterix', 'Poznamka pre Asterix', 'OPEN', 2);
INSERT INTO project_tm (id) VALUES (3);

-- ORDERS INSERTS (4)
INSERT INTO order_general (created, order_number) VALUES (current_timestamp, 10);
INSERT INTO order_general (created, order_number) VALUES (current_timestamp, 20);
INSERT INTO order_general (created, order_number) VALUES (current_timestamp, 30);
INSERT INTO order_general (created, order_number) VALUES (current_timestamp, 40);
INSERT INTO order_general (created, order_number) VALUES (current_timestamp, 50);
INSERT INTO order_general (created, order_number) VALUES (current_timestamp, 60);
INSERT INTO order_tm (id, currency, number_md, rate_md, order_type, valid_from, valid_to, main_order_id, project_id, subscriber_id, supplier_id) VALUES (1, 'EUR', 10000, 5500, 'INCOMING', '2015-06-01', '2020-06-01', 1, 1, 1, 2);
INSERT INTO order_tm (id, currency, number_md, rate_md, order_type, valid_from, valid_to, main_order_id, project_id, subscriber_id, supplier_id) VALUES (2, 'EUR', 14050, 5500, 'INCOMING', '2015-06-01', '2020-06-01', 2, 1, 2, 2);
INSERT INTO order_tm (id, currency, number_md, rate_md, order_type, valid_from, valid_to, main_order_id, project_id, subscriber_id, supplier_id) VALUES (3, 'EUR', 12000, 5500, 'INCOMING', '2015-06-01', '2020-06-01', 2, 1, 2, 1);
INSERT INTO order_tm (id, currency, number_md, rate_md, order_type, valid_from, valid_to, main_order_id, project_id, subscriber_id, supplier_id) VALUES (4, 'EUR', 50000, 5500, 'INCOMING', '2015-06-01', '2020-06-01', 1, 2, 1, 2);
INSERT INTO order_tm (id, currency, number_md, rate_md, order_type, valid_from, valid_to, main_order_id, project_id, subscriber_id, supplier_id) VALUES (5, 'EUR', 50000, 5500, 'INCOMING', '2015-06-01', '2020-06-01', 2, 2, 1, 1);
INSERT INTO order_tm (id, currency, number_md, rate_md, order_type, valid_from, valid_to, main_order_id, project_id, subscriber_id, supplier_id) VALUES (6, 'EUR', 50000, 5500, 'INCOMING', '2015-06-01', '2020-06-01', 2, 1, 2, 2);

-- DELIVERIES INSERTS
INSERT INTO delivery_tm (created, number_md, period, order_id) VALUES ('2017-11-26', 9000, '2018-11-26', 1);
INSERT INTO delivery_tm (created, number_md, period, order_id) VALUES ('2017-11-26', 4501, '2018-11-26', 1);
INSERT INTO delivery_tm (created, number_md, period, order_id) VALUES ('2017-11-26', 6697, '2018-11-26', 2);
INSERT INTO delivery_tm (created, number_md, period, order_id) VALUES ('2017-11-26', 11000, '2018-11-26', 2);