package cz.ita.javaee.web.controller;

import cz.ita.javaee.dto.TmOrderDto;
import cz.ita.javaee.service.api.TmOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/tm-order")
public class TmOrderController {

    @Autowired
    private TmOrderService tmOrderService;

    @RequestMapping(path="", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    public TmOrderDto create(@Valid @RequestBody TmOrderDto tmOrderDto) {
        return tmOrderService.create(tmOrderDto);
    }

    @RequestMapping(path="", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.PUT)
    public TmOrderDto update(@Valid @RequestBody TmOrderDto tmOrderDto) {
        return tmOrderService.update(tmOrderDto);
    }

    @RequestMapping(path="/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable Long id) {
        tmOrderService.delete(id);
    }

}
