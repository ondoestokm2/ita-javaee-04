package cz.ita.javaee.web.controller;

import cz.ita.javaee.dto.TmDeliveryDto;
import cz.ita.javaee.service.api.TmDeliveryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/tm-delivery")
public class TmDeliveryController {

    @Autowired
    private TmDeliveryService tmDeliveryService;

    @RequestMapping(path="", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    public TmDeliveryDto create(@Valid @RequestBody TmDeliveryDto tmDeliveryDto) {
        return tmDeliveryService.create(tmDeliveryDto);
    }

    @RequestMapping(path="", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.PUT)
    public TmDeliveryDto update(@Valid @RequestBody TmDeliveryDto tmDeliveryDto) {
        return tmDeliveryService.update(tmDeliveryDto);
    }

    @RequestMapping(path="/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable Long id) {
        tmDeliveryService.delete(id);
    }

}
