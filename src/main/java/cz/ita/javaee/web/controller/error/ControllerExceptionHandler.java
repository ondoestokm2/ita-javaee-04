package cz.ita.javaee.web.controller.error;

import com.google.common.base.Throwables;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

@ControllerAdvice
public class ControllerExceptionHandler {

    @ExceptionHandler(Exception.class)
    public @ResponseBody
    ResponseEntity<Map<String, Object>> handleException(Exception ex) {
        Map<String, Object> errorInfo = new HashMap<>();
        errorInfo.put("message", ex.getMessage());
        errorInfo.put("stackTrace", Throwables.getStackTraceAsString(ex));

        return new ResponseEntity<Map<String, Object>>(errorInfo, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public @ResponseBody
    ResponseEntity<Map<String, Object>> handleIOException(MethodArgumentNotValidException ex) {
        Map<String, Object> errorInfo = new HashMap<>();
        errorInfo.put("message", "Validation failed");
        errorInfo.put("stackTrace", Throwables.getStackTraceAsString(ex));
        errorInfo.put("invalidFields", ex.getBindingResult().getFieldErrors().stream().map(FieldError::getField).collect(Collectors.toList()));

        return new ResponseEntity<Map<String, Object>>(errorInfo, HttpStatus.BAD_REQUEST);
    }
}
