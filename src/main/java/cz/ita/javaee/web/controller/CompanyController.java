package cz.ita.javaee.web.controller;

import cz.ita.javaee.dto.CompanyDto;
import cz.ita.javaee.service.api.AresCompanyService;
import cz.ita.javaee.service.api.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/company")
public class CompanyController {

    @Autowired
    private CompanyService companyService;

    @Autowired
    private AresCompanyService aresService;

    @RequestMapping(path="/ares", method = RequestMethod.GET)
    public List<CompanyDto> findAresCompanies(String name) throws Exception {
        return aresService.findCompany(name);
    }

    @RequestMapping(path="", method = RequestMethod.GET)
    public List<CompanyDto> findAll() {
        return companyService.findAll();
    }

    @RequestMapping(path="", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    public CompanyDto create(@Valid @RequestBody CompanyDto company) {
        return companyService.create(company);
    }

    @RequestMapping(path="", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.PUT)
    public CompanyDto update(@Valid @RequestBody CompanyDto company) {
        return companyService.update(company);
    }

    @RequestMapping(path="/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable Long id) {
        companyService.delete(id);
    }



}
