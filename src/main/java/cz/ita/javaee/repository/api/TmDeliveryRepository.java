package cz.ita.javaee.repository.api;

import cz.ita.javaee.model.TmDeliveryEntity;
import cz.ita.javaee.model.TmProjectEntity;

public interface TmDeliveryRepository extends EntityRepository<TmDeliveryEntity>{

}
