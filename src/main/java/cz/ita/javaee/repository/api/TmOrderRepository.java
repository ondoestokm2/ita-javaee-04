package cz.ita.javaee.repository.api;

import cz.ita.javaee.model.TmOrderEntity;
import cz.ita.javaee.model.TmProjectEntity;

public interface TmOrderRepository extends EntityRepository<TmOrderEntity>{
    TmOrderEntity findLastDeliveryOrder(int year);
}
