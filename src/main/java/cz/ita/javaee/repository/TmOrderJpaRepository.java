package cz.ita.javaee.repository;

import cz.ita.javaee.model.TmOrderEntity;
import cz.ita.javaee.repository.api.TmOrderRepository;
import cz.ita.javaee.type.TmOrderType;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.time.LocalDateTime;
import java.util.List;

@Repository
@Profile("jpa")
public class TmOrderJpaRepository implements TmOrderRepository {

    @PersistenceContext
    EntityManager entityManager;

    @Override
    public TmOrderEntity findOne(long id) {
        return entityManager
                .createNamedQuery("ORDER_TM_FIND_ONE", TmOrderEntity.class)
                .setParameter("pId", id)
                .getSingleResult();
    }

    @Override
    public List<TmOrderEntity> find(int offset, int limit) {
        return entityManager
                .createNamedQuery("ORDER_TM_FIND_ALL", TmOrderEntity.class)
                .setFirstResult(offset)
                .setMaxResults(limit)
                .getResultList();
    }

    @Override
    public void delete(long id) {
        entityManager.remove(entityManager.getReference(TmOrderEntity.class, id));
    }

    @Override
    public TmOrderEntity create(TmOrderEntity entity) {
        entityManager.persist(entity);
        return entity;
    }

    @Override
    public TmOrderEntity update(TmOrderEntity entity) {
        entity.setCreated(LocalDateTime.now());
        return entityManager.merge(entity);
    }

    @Override
    public TmOrderEntity findLastDeliveryOrder(int year) {
        TmOrderEntity singleResult = entityManager
                .createNamedQuery("FIND_LAST_ORDER_IN_YEAR", TmOrderEntity.class)
                .setMaxResults(1)
                .setParameter("pType", TmOrderType.DELIVERY)
                .setParameter("pYear", year)
                .getSingleResult();
        return singleResult;
    }
}
