package cz.ita.javaee.repository;

import cz.ita.javaee.model.TmDeliveryEntity;
import cz.ita.javaee.repository.api.TmDeliveryRepository;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
@Profile("jpa")
public class TmDeliveryJpaRepository implements TmDeliveryRepository {

    @PersistenceContext
    EntityManager entityManager;

    @Override
    public TmDeliveryEntity findOne(long id) {
        return entityManager
                .createNamedQuery("DELIVERY_TM_FIND_ONE", TmDeliveryEntity.class)
                .setParameter("pId", id)
                .getSingleResult();
    }

    @Override
    public List<TmDeliveryEntity> find(int offset, int limit) {
        return entityManager
                .createNamedQuery("DELIVERY_TM_FIND_ALL", TmDeliveryEntity.class)
                .setFirstResult(offset)
                .setMaxResults(limit)
                .getResultList();
    }

    @Override
    public void delete(long id) {
        entityManager.remove(entityManager.getReference(TmDeliveryEntity.class, id));
    }

    @Override
    public TmDeliveryEntity create(TmDeliveryEntity entity) {
        entityManager.persist(entity);
        return entity;

    }

    @Override
    public TmDeliveryEntity update(TmDeliveryEntity entity) {
        return entityManager.merge(entity);
    }
}
