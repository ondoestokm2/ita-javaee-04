package cz.ita.javaee.repository;

import cz.ita.javaee.model.TmProjectEntity;
import cz.ita.javaee.repository.api.TmProjectRepository;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
@Profile("jpa")
public class TmProjectJpaRepository implements TmProjectRepository {

    @PersistenceContext
    EntityManager entityManager;

    @Override
    public TmProjectEntity findOne(long id) {
            return entityManager
                    .createNamedQuery("PROJECT_TM_FIND_ONE", TmProjectEntity.class)
                    .setParameter("pId", id)
                    .getSingleResult();
    }

    @Override
    public List<TmProjectEntity> find(int offset, int limit) {
            return entityManager
                    .createNamedQuery("PROJECT_TM_FIND_ALL", TmProjectEntity.class)
                    .setFirstResult(offset)
                    .setMaxResults(limit)
                    .getResultList();
    }

    @Override
    public void delete(long id) {
        entityManager.remove(entityManager.getReference(TmProjectEntity.class, id));
    }

    @Override
    public TmProjectEntity create(TmProjectEntity entity) {
            entityManager.persist(entity);
            return entity;
    }

    @Override
    public TmProjectEntity update(TmProjectEntity entity) {
            return entityManager.merge(entity);
    }
}
