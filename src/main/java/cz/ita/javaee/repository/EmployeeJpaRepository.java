package cz.ita.javaee.repository;

import cz.ita.javaee.model.EmployeeEntity;
import cz.ita.javaee.repository.api.EmployeeRepository;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Repository
@Profile("jpa")
public class EmployeeJpaRepository implements EmployeeRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public EmployeeEntity findOne(long id) {
        return entityManager.find(EmployeeEntity.class, id);
    }

    @Override
    public List<EmployeeEntity> find(int offset, int limit) {
        Query query = entityManager.createQuery("SELECT e FROM EmployeeEntity e", EmployeeEntity.class);
        query.setFirstResult(offset);
        query.setMaxResults(limit);
        return query.getResultList();
    }

    @Override
    public void delete(long id) {
        entityManager.remove(entityManager.getReference(EmployeeEntity.class, id));
    }

    @Override
    public EmployeeEntity update(EmployeeEntity entity) {
        return entityManager.merge(entity);
    }

    @Override
    public EmployeeEntity create(EmployeeEntity entity) {
        entityManager.persist(entity);
        return entity;
    }
}
