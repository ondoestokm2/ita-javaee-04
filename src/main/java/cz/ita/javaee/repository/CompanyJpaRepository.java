package cz.ita.javaee.repository;

import cz.ita.javaee.model.CompanyEntity;
import cz.ita.javaee.model.TmOrderEntity;
import cz.ita.javaee.model.TmProjectEntity;
import cz.ita.javaee.repository.api.CompanyRepository;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Repository
@Profile("jpa")
public class CompanyJpaRepository implements CompanyRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public CompanyEntity findOne(long id) {
        return entityManager.find(CompanyEntity.class, id);
    }

    @Override
    public List<CompanyEntity> find(int offset, int limit) {
        Query query = entityManager.createQuery("SELECT c FROM CompanyEntity c", CompanyEntity.class);
        query.setFirstResult(offset);
        query.setMaxResults(limit);
        return query.getResultList();
    }

    @Override
    public void delete(long id) {
        List<TmOrderEntity> orders = entityManager.createNamedQuery("ORDER_TM_FIND_ALL_FOR_COMPANY_ID", TmOrderEntity.class)
                .setParameter("pCompanyId", id)
                .getResultList();

        List<TmProjectEntity> projects = entityManager.createNamedQuery("PROJECT_TM_FIND_ALL_FOR_COMPANY_ID", TmProjectEntity.class)
                .setParameter("pCompanyId", id)
                .getResultList();

        orders.stream().forEach(tmOrderEntity -> entityManager.remove(tmOrderEntity));
        projects.stream().forEach(tmProjectEntity -> entityManager.remove(tmProjectEntity));

        entityManager.remove(entityManager.getReference(CompanyEntity.class, id));
    }

    @Override
    public CompanyEntity update(CompanyEntity entity) {
        return entityManager.merge(entity);
    }

    @Override
    public CompanyEntity create(CompanyEntity entity) {
        entityManager.persist(entity);
        return entity;
    }
}
