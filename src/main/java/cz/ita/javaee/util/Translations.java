package cz.ita.javaee.util;

import cz.ita.javaee.dto.*;
import cz.ita.javaee.model.*;

import java.time.LocalDateTime;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Translations {
    public static Function<CompanyEntity, CompanyDto> COMPANY_DOMAIN_TO_DTO = companyEntity -> {
        if (companyEntity == null) return null;

        CompanyDto companyDto = new CompanyDto();

        companyDto.setId(companyEntity.getId());
        companyDto.setName(companyEntity.getName());
        companyDto.setCompanyId(companyEntity.getCompanyId());
        companyDto.setVatId(companyEntity.getVatId());
        companyDto.setActive(companyEntity.isActive());
        companyDto.setOwnershipped(companyEntity.isOwnershipped());
        companyDto.setCreated(companyEntity.getCreated());
        companyDto.setNote(companyEntity.getNote());
        companyDto.setAddress(new AddressDto());
        companyDto.getAddress().setStreetName(companyEntity.getAddress().getStreetName());
        companyDto.getAddress().setZipCode(companyEntity.getAddress().getZipCode());
        companyDto.getAddress().setCity(companyEntity.getAddress().getCity());
        companyDto.getAddress().setHouseNumber(companyEntity.getAddress().getHouseNumber());
        companyDto.getAddress().setCountry(companyEntity.getAddress().getCountry());

        return companyDto;
    };

    public static Function<CompanyDto, CompanyEntity> COMPANY_DTO_TO_DOMAIN = companyDto -> {
        if (companyDto == null) return null;

        CompanyEntity companyEntity = new CompanyEntity();

        companyEntity.setId(companyDto.getId());
        companyEntity.setName(companyDto.getName());
        companyEntity.setCompanyId(companyDto.getCompanyId());
        companyEntity.setVatId(companyDto.getVatId());
        companyEntity.setActive(companyDto.isActive());
        companyEntity.setOwnershipped(companyDto.isOwnershipped());
        companyEntity.setCreated(companyDto.getCreated());
        companyEntity.setNote(companyDto.getNote());
        companyEntity.setAddress(new AddressEntity());
        companyEntity.getAddress().setStreetName(companyDto.getAddress().getStreetName());
        companyEntity.getAddress().setZipCode(companyDto.getAddress().getZipCode());
        companyEntity.getAddress().setCity(companyDto.getAddress().getCity());
        companyEntity.getAddress().setHouseNumber(companyDto.getAddress().getHouseNumber());
        companyEntity.getAddress().setCountry(companyDto.getAddress().getCountry());

        return companyEntity;
    };

    public static Function<EmployeeEntity, EmployeeDto> EMPLOYEE_DOMAIN_TO_DTO = employeeEntity -> {
        if (employeeEntity == null) return null;

        EmployeeDto employeeDto = new EmployeeDto();

        employeeDto.setId(employeeEntity.getId());
        employeeDto.setFirstName(employeeEntity.getFirstName());
        employeeDto.setSurname(employeeEntity.getSurname());
        employeeDto.setActive(employeeEntity.isActive());
        employeeDto.setNote(employeeEntity.getNote());
        employeeDto.setCreated(employeeEntity.getCreated());
        employeeDto.setAddress(new AddressDto());
        employeeDto.getAddress().setStreetName(employeeEntity.getAddress().getStreetName());
        employeeDto.getAddress().setZipCode(employeeEntity.getAddress().getZipCode());
        employeeDto.getAddress().setCity(employeeEntity.getAddress().getCity());
        employeeDto.getAddress().setHouseNumber(employeeEntity.getAddress().getHouseNumber());
        employeeDto.getAddress().setCountry(employeeEntity.getAddress().getCountry());

        return employeeDto;
    };

    public static Function<EmployeeDto, EmployeeEntity> EMPLOYEE_DTO_TO_DOMAIN = employeeDto -> {
        if (employeeDto == null) return null;

        EmployeeEntity employeeEntity = new EmployeeEntity();

        employeeEntity.setId(employeeDto.getId());
        employeeEntity.setFirstName(employeeDto.getFirstName());
        employeeEntity.setSurname(employeeDto.getSurname());
        employeeEntity.setActive(employeeDto.isActive());
        employeeEntity.setNote(employeeDto.getNote());
        employeeEntity.setCreated(employeeDto.getCreated());
        employeeEntity.setAddress(new AddressEntity());
        employeeEntity.getAddress().setStreetName(employeeDto.getAddress().getStreetName());
        employeeEntity.getAddress().setZipCode(employeeDto.getAddress().getZipCode());
        employeeEntity.getAddress().setCity(employeeDto.getAddress().getCity());
        employeeEntity.getAddress().setHouseNumber(employeeDto.getAddress().getHouseNumber());
        employeeEntity.getAddress().setCountry(employeeDto.getAddress().getCountry());

        return employeeEntity;
    };

    public static Function<TmProjectEntity, TmProjectDto> TM_PROJECT_DOMAIN_TO_DTO = tmProjectEntity -> {
        if (tmProjectEntity == null) return null;

        TmProjectDto tmProjectDto = new TmProjectDto();

        tmProjectDto.setId(tmProjectEntity.getId());
        tmProjectDto.setCustomer(COMPANY_DOMAIN_TO_DTO.apply(tmProjectEntity.getCustomer()));
        tmProjectDto.setName(tmProjectEntity.getName());
        tmProjectDto.setNote(tmProjectEntity.getNote());
        tmProjectDto.setCreated(tmProjectEntity.getCreated());
        tmProjectDto.setStatus(tmProjectEntity.getStatus());
        tmProjectDto.setOrders(tmProjectEntity.getOrders().stream().map(Translations.TM_ORDER_DOMAIN_TO_DTO).collect(Collectors.toSet()));

        return tmProjectDto;
    };

    public static Function<TmProjectEntity, TmProjectDto> TM_PROJECT_DOMAIN_TO_DTO_BASE = tmProjectEntity -> {
        if (tmProjectEntity == null) return null;

        TmProjectDto tmProjectDto = new TmProjectDto();

        tmProjectDto.setId(tmProjectEntity.getId());
        tmProjectDto.setCustomer(COMPANY_DOMAIN_TO_DTO.apply(tmProjectEntity.getCustomer()));
        tmProjectDto.setName(tmProjectEntity.getName());
        tmProjectDto.setNote(tmProjectEntity.getNote());
        tmProjectDto.setCreated(tmProjectEntity.getCreated());
        tmProjectDto.setStatus(tmProjectEntity.getStatus());
        tmProjectDto.setOrders(tmProjectEntity.getOrders().stream().map(Translations.TM_ORDER_DOMAIN_TO_DTO_BASE).collect(Collectors.toSet()));

        return tmProjectDto;
    };

    public static Function<TmProjectEntity, TmProjectDto> TM_PROJECT_DOMAIN_TO_DTO_EXTRA_BASE = tmProjectEntity -> {
        if (tmProjectEntity == null) return null;

        TmProjectDto tmProjectDto = new TmProjectDto();

        tmProjectDto.setId(tmProjectEntity.getId());
        tmProjectDto.setCustomer(COMPANY_DOMAIN_TO_DTO.apply(tmProjectEntity.getCustomer()));
        tmProjectDto.setName(tmProjectEntity.getName());
        tmProjectDto.setNote(tmProjectEntity.getNote());
        tmProjectDto.setCreated(tmProjectEntity.getCreated());
        tmProjectDto.setStatus(tmProjectEntity.getStatus());
        //tmProjectDto.setOrders(new HashSet<>()); // TODO NULL?
        tmProjectDto.setOrders(null);
        return tmProjectDto;
    };

    public static Function<TmProjectDto, TmProjectEntity> TM_PROJECT_DTO_TO_DOMAIN = tmProjectDto -> {
        if (tmProjectDto == null) return null;

        TmProjectEntity tmProjectEntity = new TmProjectEntity();

        tmProjectEntity.setId(tmProjectDto.getId());
        tmProjectEntity.setCustomer(COMPANY_DTO_TO_DOMAIN.apply(tmProjectDto.getCustomer()));
        tmProjectEntity.setName(tmProjectDto.getName());
        tmProjectEntity.setNote(tmProjectDto.getNote());
        tmProjectEntity.setCreated(tmProjectDto.getCreated());
        tmProjectEntity.setStatus(tmProjectDto.getStatus());

        tmProjectEntity.setOrders(tmProjectDto.getOrders()
                .stream()
                .map(Translations.TM_ORDER_DTO_TO_DOMAIN)
                .filter(Objects::nonNull)
                .collect(Collectors.toSet()));

        return tmProjectEntity;
    };

    public static Function<TmProjectDto, TmProjectEntity> TM_PROJECT_DTO_TO_DOMAIN_BASE = tmProjectDto -> {
        if (tmProjectDto == null) return null;

        TmProjectEntity tmProjectEntity = new TmProjectEntity();

        tmProjectEntity.setId(tmProjectDto.getId());
        tmProjectEntity.setCustomer(COMPANY_DTO_TO_DOMAIN.apply(tmProjectDto.getCustomer()));
        tmProjectEntity.setName(tmProjectDto.getName());
        tmProjectEntity.setNote(tmProjectDto.getNote());
        tmProjectEntity.setCreated(tmProjectDto.getCreated());
        tmProjectEntity.setStatus(tmProjectDto.getStatus());
        tmProjectEntity.setOrders(tmProjectDto.getOrders().stream().map(Translations.TM_ORDER_DTO_TO_DOMAIN_BASE).collect(Collectors.toSet()));

        return tmProjectEntity;
    };

    public static Function<TmProjectDto, TmProjectEntity> TM_PROJECT_DTO_TO_DOMAIN_EXTRA_BASE = tmProjectDto -> {
        if (tmProjectDto == null) return null;

        TmProjectEntity tmProjectEntity = new TmProjectEntity();

        tmProjectEntity.setId(tmProjectDto.getId());
        tmProjectEntity.setCustomer(COMPANY_DTO_TO_DOMAIN.apply(tmProjectDto.getCustomer()));
        tmProjectEntity.setName(tmProjectDto.getName());
        tmProjectEntity.setNote(tmProjectDto.getNote());
        tmProjectEntity.setCreated(tmProjectDto.getCreated());
        tmProjectEntity.setStatus(tmProjectDto.getStatus());
        //tmProjectDto.setOrders(new HashSet<>()); // TODO NULL?
        tmProjectEntity.setOrders(null);

        return tmProjectEntity;
    };

    public static Function<TmOrderEntity, TmOrderDto> TM_ORDER_DOMAIN_TO_DTO = tmOrderEntity -> {
        if (tmOrderEntity == null) return null;

        TmOrderDto tmOrderDto = new TmOrderDto();

        tmOrderDto.setId(tmOrderEntity.getId());
        tmOrderDto.setCreated(tmOrderEntity.getCreated());
        tmOrderDto.setNumber(tmOrderEntity.getNumber());
        tmOrderDto.setMdRate(tmOrderEntity.getMdRate());
        tmOrderDto.setMdNumber(tmOrderEntity.getMdNumber());
        tmOrderDto.setCurrency(tmOrderEntity.getCurrency());
        tmOrderDto.setValidFrom(tmOrderEntity.getValidFrom());
        tmOrderDto.setValidTo(tmOrderEntity.getValidTo());
        tmOrderDto.setType(tmOrderEntity.getType());
        tmOrderDto.setMainOrder(Translations.TM_ORDER_DOMAIN_TO_DTO_BASE.apply(tmOrderEntity.getMainOrder()));
        tmOrderDto.setProject(Translations.TM_PROJECT_DOMAIN_TO_DTO_BASE.apply(tmOrderEntity.getProject()));//
        tmOrderDto.setSubscriber(Translations.COMPANY_DOMAIN_TO_DTO.apply(tmOrderEntity.getSubscriber()));
        tmOrderDto.setSupplier(Translations.COMPANY_DOMAIN_TO_DTO.apply(tmOrderEntity.getSupplier()));
        tmOrderDto.setSubOrders(
                tmOrderEntity.getSubOrders()
                        .stream()
                        .map(Translations.TM_ORDER_DOMAIN_TO_DTO_BASE)
                        .collect(Collectors.toSet()));
        tmOrderDto.setDeliveries(
                tmOrderEntity.getDeliveries()
                        .stream()
                        .map(Translations.TM_DELIVERY_DOMAIN_TO_DTO)
                        .collect(Collectors.toSet()));

        return tmOrderDto;
    };

    public static Function<TmOrderEntity, TmOrderDto> TM_ORDER_DOMAIN_TO_DTO_BASE = tmOrderEntity -> {
        if (tmOrderEntity == null) return null;

        TmOrderDto tmOrderDto = new TmOrderDto();

        tmOrderDto.setId(tmOrderEntity.getId());
        tmOrderDto.setNumber(tmOrderEntity.getNumber());
        tmOrderDto.setMdNumber(tmOrderEntity.getMdNumber());
        tmOrderDto.setMdRate(tmOrderEntity.getMdRate());
        tmOrderDto.setCurrency(tmOrderEntity.getCurrency());
        tmOrderDto.setValidFrom(tmOrderEntity.getValidFrom());
        tmOrderDto.setValidTo(tmOrderEntity.getValidTo());
        tmOrderDto.setType(tmOrderEntity.getType());
        tmOrderDto.setDeliveries(
                tmOrderEntity.getDeliveries()
                        .stream()
                        .map(Translations.TM_DELIVERY_DOMAIN_TO_DTO_BASE)
                        .collect(Collectors.toSet()));
        tmOrderDto.setMainOrder(Translations.TM_ORDER_DOMAIN_TO_DTO_EXTRA_BASE.apply(tmOrderEntity.getMainOrder()));
        tmOrderDto.setProject(Translations.TM_PROJECT_DOMAIN_TO_DTO_EXTRA_BASE.apply(tmOrderEntity.getProject()));
        tmOrderDto.setSubscriber(COMPANY_DOMAIN_TO_DTO.apply(tmOrderEntity.getSubscriber()));
        tmOrderDto.setSupplier(COMPANY_DOMAIN_TO_DTO.apply(tmOrderEntity.getSupplier()));

        return tmOrderDto;
    };

    public static Function<TmOrderEntity, TmOrderDto> TM_ORDER_DOMAIN_TO_DTO_EXTRA_BASE = tmOrderEntity -> {
        if (tmOrderEntity == null) return null;

        TmOrderDto tmOrderDto = new TmOrderDto();

        tmOrderDto.setId(tmOrderEntity.getId());
        tmOrderDto.setNumber(tmOrderEntity.getNumber());
        tmOrderDto.setMdNumber(tmOrderEntity.getMdNumber());
        tmOrderDto.setMdRate(tmOrderEntity.getMdRate());
        tmOrderDto.setCurrency(tmOrderEntity.getCurrency());
        tmOrderDto.setValidFrom(tmOrderEntity.getValidFrom());
        tmOrderDto.setValidTo(tmOrderEntity.getValidTo());
        tmOrderDto.setType(tmOrderEntity.getType());
        tmOrderDto.setMainOrder(null);
        tmOrderDto.setDeliveries(null); // TODO BASE OR EXTRA_BASE WITHOU CYCLE!
        tmOrderDto.setProject(Translations.TM_PROJECT_DOMAIN_TO_DTO_EXTRA_BASE.apply(tmOrderEntity.getProject())); // TODO NULL ?
        tmOrderDto.setSubscriber(COMPANY_DOMAIN_TO_DTO.apply(tmOrderEntity.getSubscriber()));
        tmOrderDto.setSupplier(COMPANY_DOMAIN_TO_DTO.apply(tmOrderEntity.getSupplier()));

        return tmOrderDto;
    };

    public static Function<TmOrderDto, TmOrderEntity> TM_ORDER_DTO_TO_DOMAIN = tmOrderDto -> {
        if (tmOrderDto == null) return null;

        TmOrderEntity tmOrderEntity = new TmOrderEntity();

        tmOrderEntity.setId(tmOrderDto.getId());
        tmOrderEntity.setCreated(tmOrderDto.getCreated());
        tmOrderEntity.setNumber(tmOrderDto.getNumber());
        tmOrderEntity.setMdNumber(tmOrderDto.getMdNumber());
        tmOrderEntity.setMdRate(tmOrderDto.getMdRate());
        tmOrderEntity.setCurrency(tmOrderDto.getCurrency());
        tmOrderEntity.setValidFrom(tmOrderDto.getValidFrom());
        tmOrderEntity.setValidTo(tmOrderDto.getValidTo());
        tmOrderEntity.setType(tmOrderDto.getType());
        tmOrderEntity.setMainOrder(Translations.TM_ORDER_DTO_TO_DOMAIN_BASE.apply(tmOrderDto.getMainOrder()));
        tmOrderEntity.setProject(Translations.TM_PROJECT_DTO_TO_DOMAIN_BASE.apply(tmOrderDto.getProject()));
        tmOrderEntity.setSubscriber(Translations.COMPANY_DTO_TO_DOMAIN.apply(tmOrderDto.getSubscriber()));
        tmOrderEntity.setSupplier(Translations.COMPANY_DTO_TO_DOMAIN.apply(tmOrderDto.getSupplier()));
        tmOrderEntity.setSubOrders(
                tmOrderDto.getSubOrders()
                        .stream()
                        .map(Translations.TM_ORDER_DTO_TO_DOMAIN_BASE)
                        .collect(Collectors.toSet()));
        tmOrderEntity.setDeliveries(
                tmOrderDto.getDeliveries()
                        .stream()
                        .map(Translations.TM_DELIVERY_DTO_TO_DOMAIN)
                        .collect(Collectors.toSet()));

        return tmOrderEntity;
    };

    public static Function<TmOrderDto, TmOrderEntity> TM_ORDER_DTO_TO_DOMAIN_BASE = tmOrderDto -> {
        if (tmOrderDto == null) return null;

        TmOrderEntity tmOrderEntity = new TmOrderEntity();

        tmOrderEntity.setId(tmOrderDto.getId());
        tmOrderEntity.setNumber(tmOrderDto.getNumber());
        tmOrderEntity.setMdNumber(tmOrderDto.getMdNumber());
        tmOrderEntity.setMdRate(tmOrderDto.getMdRate());
        tmOrderEntity.setCurrency(tmOrderDto.getCurrency());
        tmOrderEntity.setValidFrom(tmOrderDto.getValidFrom());
        tmOrderEntity.setValidTo(tmOrderDto.getValidTo());
        tmOrderEntity.setType(tmOrderDto.getType());

//        tmOrderEntity.setDeliveries(
//                Optional.ofNullable(tmOrderDto.getDeliveries())
//                        .orElseGet(HashSet::new)
//                        .stream()
//                        .map(Translations.TM_DELIVERY_DTO_TO_DOMAIN)
//                        .filter(Objects::nonNull)
//                        .collect(Collectors.toSet()));

        tmOrderEntity.setMainOrder(Translations.TM_ORDER_DTO_TO_DOMAIN_EXTRA_BASE.apply(tmOrderDto.getMainOrder()));
        tmOrderEntity.setProject(Translations.TM_PROJECT_DTO_TO_DOMAIN_EXTRA_BASE.apply(tmOrderDto.getProject()));
        tmOrderEntity.setSubscriber(COMPANY_DTO_TO_DOMAIN.apply(tmOrderDto.getSubscriber()));
        tmOrderEntity.setSupplier(COMPANY_DTO_TO_DOMAIN.apply(tmOrderDto.getSupplier()));

        return tmOrderEntity;
    };

    public static Function<TmOrderDto, TmOrderEntity> TM_ORDER_DTO_TO_DOMAIN_EXTRA_BASE = tmOrderDto -> {
        if (tmOrderDto == null) return null;

        TmOrderEntity tmOrderEntity = new TmOrderEntity();

        tmOrderEntity.setId(tmOrderDto.getId());
        tmOrderEntity.setNumber(tmOrderDto.getNumber());
        tmOrderEntity.setMdNumber(tmOrderDto.getMdNumber());
        tmOrderEntity.setMdRate(tmOrderDto.getMdRate());
        tmOrderEntity.setCurrency(tmOrderDto.getCurrency());
        tmOrderEntity.setValidFrom(tmOrderDto.getValidFrom());
        tmOrderEntity.setValidTo(tmOrderDto.getValidTo());
        tmOrderEntity.setType(tmOrderDto.getType());
        tmOrderEntity.setMainOrder(null);
        tmOrderEntity.setDeliveries(null);
        tmOrderEntity.setProject(Translations.TM_PROJECT_DTO_TO_DOMAIN_EXTRA_BASE.apply(tmOrderDto.getProject()));
        tmOrderEntity.setSubscriber(COMPANY_DTO_TO_DOMAIN.apply(tmOrderDto.getSubscriber()));
        tmOrderEntity.setSupplier(COMPANY_DTO_TO_DOMAIN.apply(tmOrderDto.getSupplier()));

        return tmOrderEntity;
    };

    public static Function<TmDeliveryEntity, TmDeliveryDto> TM_DELIVERY_DOMAIN_TO_DTO = tmDeliveryEntity -> {
        if (tmDeliveryEntity == null) return null;

        TmDeliveryDto tmDeliveryDto = new TmDeliveryDto();

        tmDeliveryDto.setId(tmDeliveryEntity.getId());
        tmDeliveryDto.setMdNumber(tmDeliveryEntity.getMdNumber());
        tmDeliveryDto.setPeriod(tmDeliveryEntity.getPeriod());
        tmDeliveryDto.setCreated(tmDeliveryEntity.getCreated());
        tmDeliveryDto.setOrder(Translations.TM_ORDER_DOMAIN_TO_DTO_BASE.apply(tmDeliveryEntity.getOrder()));

        return tmDeliveryDto;
    };

    public static Function<TmDeliveryEntity, TmDeliveryDto> TM_DELIVERY_DOMAIN_TO_DTO_BASE = tmDeliveryEntity -> {
        if (tmDeliveryEntity == null) return null;

        TmDeliveryDto tmDeliveryDto = new TmDeliveryDto();

        tmDeliveryDto.setId(tmDeliveryEntity.getId());
        tmDeliveryDto.setMdNumber(tmDeliveryEntity.getMdNumber());
        tmDeliveryDto.setPeriod(tmDeliveryEntity.getPeriod());
        tmDeliveryDto.setCreated(tmDeliveryEntity.getCreated());
        tmDeliveryDto.setOrder(Translations.TM_ORDER_DOMAIN_TO_DTO_EXTRA_BASE.apply(tmDeliveryEntity.getOrder()));

        return tmDeliveryDto;
    };

    public static Function<TmDeliveryDto, TmDeliveryEntity> TM_DELIVERY_DTO_TO_DOMAIN = tmDeliveryDto -> {
        if (tmDeliveryDto == null) return null;

        TmDeliveryEntity tmDeliveryEntity = new TmDeliveryEntity();

        tmDeliveryEntity.setId(tmDeliveryDto.getId());
        tmDeliveryEntity.setMdNumber(tmDeliveryDto.getMdNumber());
        tmDeliveryEntity.setPeriod(tmDeliveryDto.getPeriod());
        tmDeliveryEntity.setCreated(LocalDateTime.now());
        tmDeliveryEntity.setOrder(Translations.TM_ORDER_DTO_TO_DOMAIN_BASE.apply(tmDeliveryDto.getOrder()));

        return tmDeliveryEntity;
    };

    public static Function<TmDeliveryDto, TmDeliveryEntity> TM_DELIVERY_DTO_TO_DOMAIN_BASE = tmDeliveryDto -> {
        if (tmDeliveryDto == null) return null;

        TmDeliveryEntity tmDeliveryEntity = new TmDeliveryEntity();

        tmDeliveryEntity.setId(tmDeliveryDto.getId());
        tmDeliveryEntity.setMdNumber(tmDeliveryDto.getMdNumber());
        tmDeliveryEntity.setPeriod(tmDeliveryDto.getPeriod());
        tmDeliveryEntity.setCreated(tmDeliveryDto.getCreated());
        tmDeliveryEntity.setOrder(Translations.TM_ORDER_DTO_TO_DOMAIN_BASE.apply(tmDeliveryDto.getOrder()));

        return tmDeliveryEntity;
    };
}
