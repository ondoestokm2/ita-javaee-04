package cz.ita.javaee.util;

import org.springframework.beans.DirectFieldAccessor;
import org.springframework.oxm.XmlMappingException;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.oxm.mime.MimeContainer;
import org.springframework.ws.soap.saaj.SaajSoapMessage;

import javax.xml.bind.JAXBException;
import javax.xml.transform.Source;

public class AresJaxb2Marshaller extends Jaxb2Marshaller {

    @Override
    public Object unmarshal(Source source) throws XmlMappingException {
        return super.unmarshal(source, null);
    }
    @Override
    public Object unmarshal(Source source, MimeContainer mimeContainer)
            throws XmlMappingException {

        Object mimeMessage = new DirectFieldAccessor(mimeContainer)
                .getPropertyValue("mimeMessage");
        Object unmarshalObject = null;
        if (mimeMessage instanceof SaajSoapMessage) {
            SaajSoapMessage soapMessage = (SaajSoapMessage) mimeMessage;
            String faultReason = soapMessage.getFaultReason();
            if (faultReason != null) {
                throw convertJaxbException(new JAXBException(faultReason));
            } else {
                unmarshalObject = super.unmarshal(source, mimeContainer);
            }
        }
        return unmarshalObject;
    }
}
