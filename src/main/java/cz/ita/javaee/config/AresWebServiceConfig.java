package cz.ita.javaee.config;

import cz.ita.javaee.util.AresJaxb2Marshaller;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.ws.client.core.WebServiceTemplate;

@Configuration
public class AresWebServiceConfig {

    @Value("${ares.package}")
    private String aresPackage;

    @Value("${ares.uri}")
    private String aresUri;

    @Bean
    public AresJaxb2Marshaller jaxb2Marshaller() {
        AresJaxb2Marshaller jaxb2Marshaller = new AresJaxb2Marshaller();
        jaxb2Marshaller.setPackagesToScan(aresPackage);
        return jaxb2Marshaller;
    }

    @Bean
    public WebServiceTemplate webServiceTemplate() {
        WebServiceTemplate webServiceTemplate = new WebServiceTemplate();
        webServiceTemplate.setMarshaller(jaxb2Marshaller());
        webServiceTemplate.setUnmarshaller(jaxb2Marshaller());
        webServiceTemplate.setDefaultUri(aresUri);

        return webServiceTemplate;
    }
}
