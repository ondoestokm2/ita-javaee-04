package cz.ita.javaee.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import java.util.Properties;

@PropertySource("classpath:mail.properties")
public class MailSenderConfiguration {

    @Value("${mail.smtp.host}")
    private String mailHost;

    @Value("${mail.smtp.port}")
    private int mailPort;

    @Value("${mail.username}")
    private String mailUsername;

    @Value("${mail.password}")
    private String mailPassword;

    @Value("${mail.protocol}")
    private String mailProtocol;

    @Value("${mail.smtp.auth}")
    private boolean mailAuth;

    @Value("${mail.smtp.starttls.enable}")
    private String mailEnableTls;

    @Value("${mail.smtp.debug}")
    private boolean mailDebug;

    @Bean
    public JavaMailSender getMailSender() {
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();

        mailSender.setHost(mailHost);
        mailSender.setPort(mailPort);
        mailSender.setUsername(mailUsername);
        mailSender.setPassword(mailPassword);

        Properties mailProperties = new Properties();
        mailProperties.put("mail.transport.protocol", mailProtocol);
        mailProperties.put("mail.smtp.auth", mailAuth);
        mailProperties.put("mail.smtp.starttls.enable", mailEnableTls);
        mailProperties.put("mail.smtp.debug", mailDebug);
        mailSender.setJavaMailProperties(mailProperties);

        return mailSender;
    }
}