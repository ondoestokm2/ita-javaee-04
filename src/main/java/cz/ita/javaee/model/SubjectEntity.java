package cz.ita.javaee.model;

import javax.persistence.*;
import javax.validation.constraints.Size;

@Entity
@Table(name = "SUBJECT")
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class SubjectEntity extends AbstractEntity {

    @Column(name = "ACTIVE")
    private boolean active = true;

    @Column(name = "NOTE", length = 300)
    @Size(max = 300)
    private String note;

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
