package cz.ita.javaee.model;

import org.hibernate.validator.constraints.Range;

import javax.persistence.*;
import java.time.LocalDate;


@NamedQueries({
        @NamedQuery(
                name = "DELIVERY_TM_FIND_ONE",
                query = "SELECT d FROM TmDeliveryEntity d LEFT JOIN d.order WHERE d.id = :pId"),
        @NamedQuery(
                name = "DELIVERY_TM_FIND_ALL",
                query = "SELECT d FROM TmDeliveryEntity d LEFT JOIN d.order"),
})
@Entity
@Table(name = "DELIVERY_TM")
public class TmDeliveryEntity extends AbstractEntity {

    @Column(name = "PERIOD", nullable = false)
    private LocalDate period;

    @Column(name = "NUMBER_MD",
            nullable = false,
            columnDefinition = "integer default 0")
    @Range(min = 1)
    private int mdNumber;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ORDER_ID",
            nullable = false,
            foreignKey = @ForeignKey(name = "FK__ORDER_TM__DELIVERY_TM__ORDER_ID"),
            columnDefinition = "integer default 0")
    private TmOrderEntity order;

    public LocalDate getPeriod() {
        return period;
    }

    public void setPeriod(LocalDate period) {
        this.period = period;
    }

    public int getMdNumber() {
        return mdNumber;
    }

    public void setMdNumber(int mdNumber) {
        this.mdNumber = mdNumber;
    }

    public TmOrderEntity getOrder() {
        return order;
    }

    public void setOrder(TmOrderEntity order) {
        this.order = order;
    }

    @Override
    public String toString() {
        return "TmDeliveryEntity{" +
                "period=" + period +
                ", mdNumber=" + mdNumber +
                ", order=" + order +
                ", id=" + getId() +
                ", created=" + getCreated() +
                '}';
    }
}
