package cz.ita.javaee.model;


import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "PROJECT_TM")
@NamedQueries({
        @NamedQuery(
                name = "PROJECT_TM_FIND_ONE",
                query = "SELECT tmp FROM TmProjectEntity tmp LEFT JOIN FETCH tmp.customer LEFT JOIN FETCH tmp.orders WHERE tmp.id = :pId"),
        @NamedQuery(
                name = "PROJECT_TM_FIND_ALL",
                query = "SELECT tmp FROM TmProjectEntity tmp LEFT JOIN FETCH tmp.customer LEFT JOIN FETCH tmp.orders"),
        @NamedQuery(
                name = "PROJECT_TM_FIND_ALL_FOR_COMPANY_ID",
                query = "SELECT p FROM TmProjectEntity p WHERE p.customer.id = :pCompanyId"
        )
})
@PrimaryKeyJoinColumn(foreignKey=@ForeignKey(name = "FK__PROJECT__PROJECT_TM__PROJECT_ID"))
public class TmProjectEntity extends ProjectEntity {

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "project", cascade = CascadeType.REMOVE)
    private Set<TmOrderEntity> orders = new HashSet<>();

    public Set<TmOrderEntity> getOrders() {
        return orders;
    }

    public void setOrders(Set<TmOrderEntity> orders) {
        this.orders = orders;
    }
}
