package cz.ita.javaee.model;

import javax.persistence.*;
import javax.validation.constraints.Size;

@Entity
@Table(name = "ORDER_GENERAL")
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class OrderEntity extends AbstractEntity {

    @Column(name = "ORDER_NUMBER",
            length = 20,
            nullable = false)
    @Size(min = 1, max = 20)
    private String number;

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
}
