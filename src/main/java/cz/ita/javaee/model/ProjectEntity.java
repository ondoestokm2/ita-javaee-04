package cz.ita.javaee.model;

import cz.ita.javaee.type.ProjectStatus;

import javax.persistence.*;
import javax.validation.constraints.Size;

@Entity
@Table(name = "PROJECT")
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class ProjectEntity extends AbstractEntity {

    @Column(name = "NAME", nullable = false, length = 100)
    @Size(min = 3, max = 100)
    private String name;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CUSTOMER_ID",
            foreignKey = @ForeignKey(name = "FK__COMPANY__PROJECT__CUSTOMER_ID"),
            columnDefinition="integer")
    private CompanyEntity customer;

    @Column(name= "PROJECT_STATUS")
    @Enumerated(EnumType.STRING)
    private ProjectStatus status = ProjectStatus.OPEN;

    @Column(name= "NOTE", length = 300)
    @Size(max = 300)
    private String note;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CompanyEntity getCustomer() {
        return customer;
    }

    public void setCustomer(CompanyEntity customer) {
        this.customer = customer;
    }

    public ProjectStatus getStatus() {
        return status;
    }

    public void setStatus(ProjectStatus status) {
        this.status = status;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
