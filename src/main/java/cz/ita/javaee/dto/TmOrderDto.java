package cz.ita.javaee.dto;

import cz.ita.javaee.type.TmOrderType;
import org.hibernate.validator.constraints.Range;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

public class TmOrderDto extends OrderDto {

    @Valid
    private TmProjectDto project;

    @NotNull
    private TmOrderType type;

    @Valid
    private CompanyDto supplier;

    @Valid
    private CompanyDto subscriber;

    @NotNull
    private LocalDate validFrom;

    @NotNull
    private LocalDate validTo;

    @NotNull
    @Range(min = 1)
    private int mdNumber;

    @NotNull
    @Range(min = 1)
    private int mdRate;

    @NotNull
    @Size(min = 1)
    private String currency;

    @Valid
    private TmOrderDto mainOrder;

    private Set<TmOrderDto> subOrders = new HashSet<>();
    private Set<TmDeliveryDto> deliveries = new HashSet<>();

    public TmProjectDto getProject() {
        return project;
    }

    public void setProject(TmProjectDto project) {
        this.project = project;
    }

    public TmOrderType getType() {
        return type;
    }

    public void setType(TmOrderType type) {
        this.type = type;
    }

    public CompanyDto getSupplier() {
        return supplier;
    }

    public void setSupplier(CompanyDto supplier) {
        this.supplier = supplier;
    }

    public CompanyDto getSubscriber() {
        return subscriber;
    }

    public void setSubscriber(CompanyDto subscriber) {
        this.subscriber = subscriber;
    }

    public LocalDate getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(LocalDate validFrom) {
        this.validFrom = validFrom;
    }

    public LocalDate getValidTo() {
        return validTo;
    }

    public void setValidTo(LocalDate validTo) {
        this.validTo = validTo;
    }

    public int getMdNumber() {
        return mdNumber;
    }

    public void setMdNumber(int mdNumber) {
        this.mdNumber = mdNumber;
    }

    public int getMdRate() {
        return mdRate;
    }

    public void setMdRate(int mdRate) {
        this.mdRate = mdRate;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public TmOrderDto getMainOrder() {
        return mainOrder;
    }

    public void setMainOrder(TmOrderDto mainOrder) {
        this.mainOrder = mainOrder;
    }

    public Set<TmOrderDto> getSubOrders() {
        return subOrders;
    }

    public void setSubOrders(Set<TmOrderDto> subOrders) {
        this.subOrders = subOrders;
    }

    public Set<TmDeliveryDto> getDeliveries() {
        return deliveries;
    }

    public void setDeliveries(Set<TmDeliveryDto> deliveries) {
        this.deliveries = deliveries;
    }

    @Override
    public String toString() {
        return "TmOrderDto{" +
                "project=" + project +
                ", type=" + type +
                ", supplier=" + supplier +
                ", subscriber=" + subscriber +
                ", validFrom=" + validFrom +
                ", validTo=" + validTo +
                ", mdNumber=" + mdNumber +
                ", mdRate=" + mdRate +
                ", currency='" + currency + '\'' +
                ", mainOrder=" + mainOrder +
                ", subOrders=" + subOrders +
                ", deliveries=" + deliveries +
                ", number='" + getNumber() + '\'' +
                ", id=" + getId() +
                ", created=" + getCreated() +
                '}';
    }
}
