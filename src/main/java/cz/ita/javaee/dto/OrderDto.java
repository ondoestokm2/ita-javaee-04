package cz.ita.javaee.dto;

import javax.validation.constraints.Size;

public abstract class OrderDto extends AbstractDto {

    @Size(min = 1, max = 20)
    private String number;

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
}
