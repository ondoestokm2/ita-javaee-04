package cz.ita.javaee.dto;

import cz.ita.javaee.type.ProjectStatus;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public abstract class ProjectDto extends AbstractDto {

    @NotNull
    @Size(min = 3, max = 100)
    private String name;

    @Valid
    private CompanyDto customer;

    @NotNull
    private ProjectStatus status = ProjectStatus.OPEN;

    @Size(max = 300)
    private String note;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CompanyDto getCustomer() {
        return customer;
    }

    public void setCustomer(CompanyDto customer) {
        this.customer = customer;
    }

    public ProjectStatus getStatus() {
        return status;
    }

    public void setStatus(ProjectStatus status) {
        this.status = status;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
