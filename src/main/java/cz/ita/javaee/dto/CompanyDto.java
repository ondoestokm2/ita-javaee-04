package cz.ita.javaee.dto;

import org.hibernate.validator.constraints.Length;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class CompanyDto extends SubjectDto {

    @NotNull
    @Length(max = 100)
    private String name;

    @NotNull
    @Size(min = 8, max = 8)
    private String companyId;

    @Size(max = 10)
    private String vatId;

    @Valid
    private AddressDto address;

    @NotNull
    private boolean ownershipped;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getVatId() {
        return vatId;
    }

    public void setVatId(String vatId) {
        this.vatId = vatId;
    }

    public AddressDto getAddress() {
        return address;
    }

    public void setAddress(AddressDto address) {
        this.address = address;
    }

    public boolean isOwnershipped() {
        return ownershipped;
    }

    public void setOwnershipped(boolean ownershipped) {
        this.ownershipped = ownershipped;
    }
}
