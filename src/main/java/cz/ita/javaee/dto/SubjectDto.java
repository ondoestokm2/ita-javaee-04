package cz.ita.javaee.dto;

import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

public class SubjectDto extends AbstractDto {

    @NotNull
    private boolean active;

    @Length(max = 300)
    private String note;

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
