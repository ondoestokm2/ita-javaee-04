package cz.ita.javaee.dto;

import org.hibernate.validator.constraints.Range;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

public class TmDeliveryDto extends AbstractDto{

    @NotNull
    private LocalDate period;

    @NotNull
    @Range(min = 1)
    private int mdNumber;

    @Valid
    private TmOrderDto order;

    public LocalDate getPeriod() {
        return period;
    }

    public void setPeriod(LocalDate period) {
        this.period = period;
    }

    public int getMdNumber() {
        return mdNumber;
    }

    public void setMdNumber(int mdNumber) {
        this.mdNumber = mdNumber;
    }

    public TmOrderDto getOrder() {
        return order;
    }

    public void setOrder(TmOrderDto order) {
        this.order = order;
    }

    @Override
    public String toString() {
        return "TmDeliveryDto{" +
                "period=" + period +
                ", mdNumber=" + mdNumber +
                ", order=" + order +
                ", id=" + getId() +
                ", created=" + getCreated() +
                '}';
    }
}
