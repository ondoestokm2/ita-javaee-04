package cz.ita.javaee.service;

import cz.ita.javaee.dto.EmployeeDto;
import cz.ita.javaee.util.Translations;
import cz.ita.javaee.model.EmployeeEntity;
import cz.ita.javaee.repository.api.EmployeeRepository;
import cz.ita.javaee.service.api.EmployeeService;
import cz.ita.javaee.service.api.NotificationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private NotificationService notificationService;

    private static Logger LOGGER = LoggerFactory.getLogger(CompanyServiceImpl.class);

    @Override
    @Transactional
    public List<EmployeeDto> findAll() {
        List<EmployeeEntity> entities = employeeRepository.find(0, Integer.MAX_VALUE);
        List<EmployeeDto> employeeDtosResult = entities.stream().map(Translations.EMPLOYEE_DOMAIN_TO_DTO).collect(Collectors.toList());
        return employeeDtosResult;
    }

    @Override
    @Transactional
    public EmployeeDto create(EmployeeDto employeeDto) {
        if (employeeDto != null) {
            employeeDto.setCreated(LocalDateTime.now());
        }
        EmployeeDto employeeDtoResult =
                Translations.EMPLOYEE_DOMAIN_TO_DTO.apply(
                        employeeRepository.create(Translations.EMPLOYEE_DTO_TO_DOMAIN.apply(employeeDto)));
        if (employeeDtoResult != null) {
            notificationService.sendNotifiction("ITA - Notification Service", "BrainIS - Employee was created!");
        }
        return employeeDtoResult;
    }

    @Override
    @Transactional
    public EmployeeDto update(EmployeeDto employeeDto) {
        EmployeeDto employeeDtoResult = Translations.EMPLOYEE_DOMAIN_TO_DTO.apply(
                employeeRepository.update(Translations.EMPLOYEE_DTO_TO_DOMAIN.apply(employeeDto)));
        return employeeDtoResult;
    }

    @Override
    @Transactional
    public void delete(Long id) {
        employeeRepository.delete(id);
    }
}
