package cz.ita.javaee.service;

import cz.ita.javaee.dto.CompanyDto;
import cz.ita.javaee.util.Translations;
import cz.ita.javaee.model.CompanyEntity;
import cz.ita.javaee.repository.api.CompanyRepository;
import cz.ita.javaee.service.api.CompanyService;
import cz.ita.javaee.service.api.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CompanyServiceImpl implements CompanyService {

    @Autowired
    private CompanyRepository companyRepository;

    @Autowired
    private NotificationService notificationService;

    @Override
    @Transactional(readOnly = true)
    public List<CompanyDto> findAll() {
        List<CompanyEntity> entities = companyRepository.find(0, Integer.MAX_VALUE);
        List<CompanyDto> companyDtosResult =  entities.stream().map(Translations.COMPANY_DOMAIN_TO_DTO).collect(Collectors.toList());
        return companyDtosResult;
    }

    @Override
    @Transactional
    public CompanyDto create(CompanyDto companyDto) {
        if (companyDto != null) {
            companyDto.setCreated(LocalDateTime.now());
        }
        CompanyDto companyDtoResult = Translations.COMPANY_DOMAIN_TO_DTO.apply(
                companyRepository.create(Translations.COMPANY_DTO_TO_DOMAIN.apply(companyDto)));
        if (companyDtoResult != null) {
            notificationService.sendNotifiction("ITA - Notification Service", "BrainIS - Company was created!");
        }
        return companyDtoResult;
    }

    @Override
    @Transactional
    public CompanyDto update(CompanyDto company) {
        CompanyDto companyDtoResult = Translations.COMPANY_DOMAIN_TO_DTO.apply(
                companyRepository.update(Translations.COMPANY_DTO_TO_DOMAIN.apply(company)));
        return companyDtoResult;
    }

    @Override
    @Transactional
    public void delete(Long id) {
        companyRepository.delete(id);
    }
}
