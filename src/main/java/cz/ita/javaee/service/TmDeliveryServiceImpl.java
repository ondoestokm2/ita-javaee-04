package cz.ita.javaee.service;

import cz.ita.javaee.dto.TmDeliveryDto;
import cz.ita.javaee.util.Translations;
import cz.ita.javaee.repository.api.TmDeliveryRepository;
import cz.ita.javaee.service.api.NotificationService;
import cz.ita.javaee.service.api.TmDeliveryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

@Service
public class TmDeliveryServiceImpl implements TmDeliveryService {

    @Autowired
    private TmDeliveryRepository tmDeliveryRepository;

    @Autowired
    private NotificationService notificationService;

    @Override
    @Transactional
    public TmDeliveryDto create(TmDeliveryDto tmDeliveryDto) {
        if (tmDeliveryDto != null) {
            tmDeliveryDto.setCreated(LocalDateTime.now());
        }
        TmDeliveryDto tmDeliveryDtoResult = Translations.TM_DELIVERY_DOMAIN_TO_DTO.apply(
                tmDeliveryRepository.create(Translations.TM_DELIVERY_DTO_TO_DOMAIN.apply(tmDeliveryDto)));
        if (tmDeliveryDtoResult != null) {
            notificationService.sendNotifiction("ITA - Notification Service", "BrainIS - TM Delivery was created!");
        }
        return tmDeliveryDtoResult;
    }

    @Override
    @Transactional
    public TmDeliveryDto update(TmDeliveryDto tmDeliveryDto) {
        TmDeliveryDto tmDeliveryDtoResult = Translations.TM_DELIVERY_DOMAIN_TO_DTO.apply(
                tmDeliveryRepository.update(Translations.TM_DELIVERY_DTO_TO_DOMAIN.apply(tmDeliveryDto)));
        return tmDeliveryDtoResult;
    }

    @Override
    @Transactional
    public void delete(Long id) {
        tmDeliveryRepository.delete(id);
    }
}
