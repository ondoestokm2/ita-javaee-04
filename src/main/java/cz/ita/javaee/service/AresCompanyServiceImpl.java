package cz.ita.javaee.service;

import com.google.common.collect.ImmutableMap;
import cz.ita.javaee.dto.AddressDto;
import cz.ita.javaee.dto.CompanyDto;
import cz.ita.javaee.service.api.AresCompanyService;
import cz.mfcr.ares.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.ws.client.core.WebServiceTemplate;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class AresCompanyServiceImpl implements AresCompanyService {

    @Value("${mail.from}")
    private String mailFrom;

    @Autowired
    private WebServiceTemplate webServiceTemplate;

    private static Map<String, String>  countryCodes = ImmutableMap.<String, String>builder()
            .put("203", "CZE")
            .put("703", "SVK")
            .build();

    @Override
    public List<CompanyDto> findCompany(String name) throws Exception {

        AresOdpovedi answer = null;

        AresDotazy dotazy = new AresDotazy();
        Dotaz dotaz = new Dotaz();
        KlicovePolozky polozky = new KlicovePolozky();

        dotazy.setDotazPocet(1);
        dotazy.setDotazTyp(AresDotazTyp.STANDARD);
        dotazy.setVystupFormat(VystupFormat.XML);
        dotazy.setUserMail(mailFrom);

        polozky.setObchodniFirma(name);

        dotaz.setPomocneID(1);
        dotaz.setTypVyhledani(AresVyberTyp.FREE);
        dotaz.setKlicovePolozky(polozky);
        dotaz.setMaxPocet(15);
        dotazy.getDotaz().add(dotaz);

        try {
            answer = (AresOdpovedi) webServiceTemplate.marshalSendAndReceive(dotazy);
        } catch (Exception e) {
            throw new Exception(e.getCause().getMessage());
        }

        if (answer.getOdpoved().get(0).getPocetZaznamu() == 0) {
            return new ArrayList<>();
        } else if (answer.getOdpoved().get(0).getPocetZaznamu() > 0) {
            return answer.getOdpoved().get(0).getZaznam().stream().map(parseCompany).collect(Collectors.toList());
        } else {
            throw new Exception(answer.getOdpoved().get(0).getError().get(0).getErrorText());
        }
    }

    private static final Function<AdresaARES, AddressDto> parseAddress = adresaARES -> {
        AddressDto addressDto = new AddressDto();

        addressDto.setCountry(countryCodes.get(adresaARES.getKodStatu()));
        addressDto.setZipCode(adresaARES.getPSC());
        addressDto.setCity(adresaARES.getNazevObce());
        addressDto.setHouseNumber(String.valueOf(adresaARES.getCisloDomovni()));
        addressDto.setStreetName(adresaARES.getNazevUlice());

        return addressDto;
    };

    private static final Function<Zaznam, CompanyDto> parseCompany = zaznam -> {
        CompanyDto companyDto = new CompanyDto();
        companyDto.setAddress(new AddressDto());

        if (zaznam.getIdentifikace().getAdresaARES() != null) {
            companyDto.setAddress(parseAddress.apply(zaznam.getIdentifikace().getAdresaARES()));
        } else if (zaznam.getIdentifikace().getOsoba() != null) {
            companyDto.setAddress(parseAddress.apply(zaznam.getIdentifikace().getOsoba().getBydliste().get(0)));
        }

        companyDto.setName(zaznam.getObchodniFirma());
        companyDto.setCompanyId(zaznam.getICO());

        return companyDto;
    };


}
