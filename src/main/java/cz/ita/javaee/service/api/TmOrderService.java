package cz.ita.javaee.service.api;

import cz.ita.javaee.dto.TmOrderDto;

public interface TmOrderService {

    TmOrderDto create(final TmOrderDto order);

    TmOrderDto update(final TmOrderDto order);

    void delete(Long id);

    TmOrderDto findLastDeliveryOrder(int year);
}
