package cz.ita.javaee.service.api;

import cz.ita.javaee.dto.CompanyDto;

import java.util.List;

public interface AresCompanyService {

    List<CompanyDto> findCompany(String name) throws Exception;
}
