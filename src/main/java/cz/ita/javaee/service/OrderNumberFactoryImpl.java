package cz.ita.javaee.service;

import cz.ita.javaee.dto.TmOrderDto;
import cz.ita.javaee.service.api.OrderNumberFactory;
import cz.ita.javaee.service.api.TmOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class OrderNumberFactoryImpl implements OrderNumberFactory {

    @Autowired
    private TmOrderService tmOrderService;

    @Override
    public String getOrderNumber() {
        TmOrderDto lastDeliveryOrder;
        String resultNumber;
        Integer orderNumber;
        try {
            lastDeliveryOrder = tmOrderService.findLastDeliveryOrder(LocalDateTime.now().getYear());
        } catch (Exception e) {
            lastDeliveryOrder = null;
        }
        if (lastDeliveryOrder != null) {
            orderNumber = Integer.parseInt(lastDeliveryOrder.getNumber().substring(4).replaceFirst("^0+(?!$)", ""));
            orderNumber++;
        } else {
            orderNumber = 1;
        }
        resultNumber = String.format("%d%04d", LocalDateTime.now().getYear(), orderNumber);
        return resultNumber;
    }
}
