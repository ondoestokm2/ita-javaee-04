package cz.ita.javaee.service;

import cz.ita.javaee.dto.TmProjectDto;
import cz.ita.javaee.util.Translations;
import cz.ita.javaee.model.TmProjectEntity;
import cz.ita.javaee.repository.api.TmProjectRepository;
import cz.ita.javaee.service.api.NotificationService;
import cz.ita.javaee.service.api.TmProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TmProjectServiceImpl implements TmProjectService {

    @Autowired
    private TmProjectRepository tmProjectRepository;

    @Autowired
    private NotificationService notificationService;

    @Override
    @Transactional
    public TmProjectDto find(Long id) {
        TmProjectEntity entity = tmProjectRepository.findOne(id);
        TmProjectDto tmProjectDtoResult = Translations.TM_PROJECT_DOMAIN_TO_DTO.apply(entity);
        return tmProjectDtoResult;
    }

    @Override
    @Transactional
    public List<TmProjectDto> findAll() {
        List<TmProjectEntity> entities = tmProjectRepository.find(0, Integer.MAX_VALUE);
        List<TmProjectDto> tmProjectDtoResult = entities
                .stream()
                .map(Translations.TM_PROJECT_DOMAIN_TO_DTO)
                .collect(Collectors.toList());
        return tmProjectDtoResult;
    }

    @Override
    @Transactional
    public TmProjectDto create(TmProjectDto tmProjectDto) {
        if (tmProjectDto != null) {
            tmProjectDto.setCreated(LocalDateTime.now());
        }
        TmProjectDto tmProjectDtoResult = Translations.TM_PROJECT_DOMAIN_TO_DTO.apply(
                tmProjectRepository.create(Translations.TM_PROJECT_DTO_TO_DOMAIN.apply(tmProjectDto)));
        if (tmProjectDtoResult != null) {
            notificationService.sendNotifiction("ITA - Notification Service", "BrainIS - TM Project was created!");
        }
        return tmProjectDtoResult;
    }

    @Override
    @Transactional
    public TmProjectDto update(TmProjectDto tmProjectDto) {
        TmProjectDto tmProjectDtoResult = Translations.TM_PROJECT_DOMAIN_TO_DTO.apply(
                tmProjectRepository.update(Translations.TM_PROJECT_DTO_TO_DOMAIN.apply(tmProjectDto)));
        return tmProjectDtoResult;
    }

    @Override
    @Transactional
    public void delete(Long id) {
        tmProjectRepository.delete(id);
    }
}
