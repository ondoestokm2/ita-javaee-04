package cz.ita.javaee.service;

import cz.ita.javaee.dto.TmOrderDto;
import cz.ita.javaee.util.Translations;
import cz.ita.javaee.model.TmOrderEntity;
import cz.ita.javaee.repository.api.TmOrderRepository;
import cz.ita.javaee.service.api.NotificationService;
import cz.ita.javaee.service.api.OrderNumberFactory;
import cz.ita.javaee.service.api.TmOrderService;
import cz.ita.javaee.type.TmOrderType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TmOrderServiceImpl implements TmOrderService {

    @Autowired
    private TmOrderRepository tmOrderRepository;

    @Autowired
    private NotificationService notificationService;

    @Autowired
    private OrderNumberFactory orderNumberFactory;

    @Transactional
    public TmOrderDto find(Long id) {
        TmOrderEntity entity = tmOrderRepository.findOne(id);
        TmOrderDto tmOrderDtoResult = Translations.TM_ORDER_DOMAIN_TO_DTO.apply(entity);
        return tmOrderDtoResult;
    }

    @Transactional
    public List<TmOrderDto> findAll() {
        List<TmOrderEntity> entities = tmOrderRepository.find(0, Integer.MAX_VALUE);
        List<TmOrderDto> tmOrderDtosResult = entities
                .stream()
                .map(Translations.TM_ORDER_DOMAIN_TO_DTO)
                .collect(Collectors.toList());
        return tmOrderDtosResult;
    }

    @Override
    @Transactional
    public TmOrderDto create(TmOrderDto tmOrderDto) {
        if (tmOrderDto != null) {
            tmOrderDto.setCreated(LocalDateTime.now());

            if (tmOrderDto.getType() == TmOrderType.DELIVERY) {
                tmOrderDto.setNumber(orderNumberFactory.getOrderNumber());
            }
        }
        TmOrderDto tmOrderDtoResult = Translations.TM_ORDER_DOMAIN_TO_DTO.apply(
                tmOrderRepository.create(Translations.TM_ORDER_DTO_TO_DOMAIN.apply(tmOrderDto)));
        if (tmOrderDtoResult != null) {
            notificationService.sendNotifiction("ITA - Notification Service", "BrainIS - TM Order was created!");
        }
        return tmOrderDtoResult;
    }

    @Override
    @Transactional
    public TmOrderDto update(TmOrderDto tmOrderDto) {
        TmOrderDto tmOrderDtoResult = Translations.TM_ORDER_DOMAIN_TO_DTO.apply(
                tmOrderRepository.update(Translations.TM_ORDER_DTO_TO_DOMAIN.apply(tmOrderDto)));
        return tmOrderDtoResult;
    }

    @Override
    @Transactional
    public void delete(Long id) {
        tmOrderRepository.delete(id);
    }

    @Override
    @Transactional(propagation= Propagation.REQUIRES_NEW)
    public TmOrderDto findLastDeliveryOrder(int year) {
        TmOrderEntity lastDeliveryOrder = tmOrderRepository.findLastDeliveryOrder(year);
        return Translations.TM_ORDER_DOMAIN_TO_DTO.apply(lastDeliveryOrder);
    }
}
