'use strict';
import notificationService from 'services/notificationService';

export default (store) => {
  return (to, from, next) => {
    if (to.meta.requiresLoggedIn && !store.getters['auth/loggedIn']) {
      next('/login');
    } else if (to.meta.requiresRole && !store.getters['auth/hasRole'](to.meta.requiresRole)) {
      notificationService.error('error.auth.invalid');
    } else {
      next()
    }
  }
};
