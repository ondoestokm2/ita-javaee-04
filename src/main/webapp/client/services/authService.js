import Vue from 'vue';
import moment from 'moment';
import {OAUTH_CLIENT_ID, OAUTH_SECRET} from '../config';

const LOGIN_URL = '/oauth/token';
const LOGOUT_URL = '/api/logout';

export default {
  async login(credentials) {
    const params = {
      'grant_type': 'password',
      'scope': 'read write',
      'username': credentials.username,
      'password': credentials.password,
      'client_id': OAUTH_CLIENT_ID,
      'client_secret': OAUTH_SECRET
    };

    const AUTH_BASIC_HEADERS = {
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        "Accept": "application/json",
        'Authorization': 'Basic ' + window.btoa(OAUTH_CLIENT_ID + ':' + OAUTH_SECRET)
      },
      emulateJSON: true
    };

    let response = await Vue.http.post(LOGIN_URL, params, AUTH_BASIC_HEADERS);

    return {
      accessToken: response.data.access_token,
      refreshToken: response.data.refresh_token,
      validTo: moment().add(response.data.expires_in, 's')
    };
  },
  async logOut() {
    let response = await Vue.http.get(LOGOUT_URL);
    return response;
  }
}
