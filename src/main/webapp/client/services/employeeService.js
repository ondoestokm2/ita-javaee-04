import _ from 'lodash';
import employeeResource from '../resources/employeeResource';
import Employee from "../domain/employee";

export default {
  newEmployee() {
    return new Employee({address: {}});
  },
  async findAll () {
    const response = await employeeResource.query();
    if (response.ok) {
      return response.data.map((employeeData) => new Employee(employeeData));
    } else {
      return null;
    }
  },
  async create (employee) {
    return employeeResource.save({}, _.pickBy(employee));
  },
  async update (employee) {
    return employeeResource.update({}, _.pickBy(employee));
  },
  async delete(id) {
    return employeeResource.delete({id: id});
  }
}
