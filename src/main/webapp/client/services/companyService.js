import companyResource from '../resources/companyResource';
import _ from 'lodash';
import Company from "../domain/company";

export default {
  newCompany() {
    return new Company({address: {}});
  },
  async findAll () {
    const response = await companyResource.query();
    if (response.ok) {
      return response.data.map((companyData) => new Company(companyData));
    } else {
      return null;
    }
  },
  async findInAres (query) {
    const response = await companyResource.ares(query);
    if (response.ok) {
      return response.data.map((companyData) => new Company(companyData));
    } else {
      return null;
    }
  },
  async create (company) {
    return companyResource.save({}, _.pickBy(company));
  },
  async update (company) {
    return companyResource.update({}, _.pickBy(company));
  },
  async delete(id) {
    return companyResource.delete({id: id});
  }
}
