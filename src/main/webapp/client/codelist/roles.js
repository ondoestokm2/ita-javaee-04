export default [
  {id: 'ADMIN', label: 'Administrátor'},
  {id: 'DEALER', label: 'Obchodník'},
  {id: 'SYSTEM_ADMIN', label: 'Systémový administrátor'},
]
