import modal from 'vue-strap/src/Modal';
import _ from 'lodash';
import notificationService from 'services/notificationService';

export default {
  components: {modalWindow: modal},
  data: () => ({
    item: null
  }),
  computed: {
    editMode() {
      return this.item && this.item.id
    }
  },
  props: {
    showModel: {
      type: Boolean,
      default: false
    },
    model: {
      type: Object
    },
    moduleName: {
      type: String,
      required: true
    }
  },
  methods: {
    createSubmit() {
      this.protect();
      this.$validator.validateAll().then( () => {
        let operation = this.moduleName;
        if (this.item.id) {
          operation += '/update';
        } else {
          operation += '/create';
        }
        this.$store.dispatch(operation, this.item).then(() => {
          notificationService.success('Položka bola uložená.');
          this.$emit('close');
        }).catch(this.unprotect);
      }).catch(this.unprotect);
    },
    clear() {
      this.unprotect();
      this.$nextTick(() => {
        this.errors.clear();
      });
    },
    cancel() {
      this.$emit('close');
    },
    onClose() {
      this.clear();
    }
  },
  watch: {
    model: function (model) {
      this.item = _.cloneDeep(model);
      this.clear();
    }
  }
}
