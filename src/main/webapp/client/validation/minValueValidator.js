import { Validator } from 'vee-validate';

/**
 * min_value validator for supporting comma "," as decimal
 */
const min_value = function (value, ref) {
  const min = ref[0];
  const valueString = value + '';

  if (Array.isArray(valueString) || valueString === null || valueString === undefined || valueString === '') {
    return false;
  }

  return Number(valueString.replace(',','.')) >= min;
};

Validator.extend('min_value2', min_value);
