import Vue from 'vue';
import currencyFilter from './currencyFilter';
import dateFilter from './dateFilter';
import dateMonthFilter from './dateMonthFilter';
import codeListValueFilter from './codeListValueFilter';

export default () => {
  Vue.filter('currency', currencyFilter);
  Vue.filter('date', dateFilter);
  Vue.filter('dateMonth', dateMonthFilter);
  Vue.filter('codeListValue', codeListValueFilter);
};
