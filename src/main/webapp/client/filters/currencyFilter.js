'use strict';

import numeral from 'numeral';
import _ from 'lodash';
import {currencies} from 'codelist/';

export default (value, currency) => {
  if (_.isUndefined(value) || value === null) {
    return null;
  } else {
    return numeral(value).format('0,0.00') + (!!currency ? ' ' + _.find(currencies, {id: currency}).label : '');
  }
}
