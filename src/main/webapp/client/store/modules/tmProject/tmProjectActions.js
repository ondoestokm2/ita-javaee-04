import * as types from "../../mutationTypes";
import tmProjectService from "../../../services/tmProjectService";

const actions = {
  async select ({state, dispatch, commit}, id) {
    dispatch('app/loadingDataEnable', null, {root: true});
    const project = await tmProjectService.find(id);
    await commit(types.TM_PROJECT_SELECT, {
      item: project
    });
    dispatch('app/loadingDataDisable', null, {root: true});
  },
  async addOrder ({commit}, newOrder) {
    await commit(types.TM_PROJECT_ADD_ORDER, {
      order: newOrder
    });
  },
  async updateOrder ({commit}, updatedOrder) {
    await commit(types.TM_PROJECT_UPDATE_ORDER, {
      order: updatedOrder
    });
  },
  async deleteOrder ({commit}, orderId) {
    await commit(types.TM_PROJECT_DELETE_ORDER, {
      orderId: orderId
    });
  },
  async addDelivery ({commit}, newDelivery) {
    console.log(newDelivery)
    await commit(types.TM_PROJECT_ADD_DELIVERY, {
      delivery: newDelivery
    });
  },
  async updateDelivery ({commit}, updatedDelivery) {
    await commit(types.TM_PROJECT_UPDATE_DELIVERY, {
      delivery: updatedDelivery
    });
  },
  async deleteDelivery ({commit}, deliveryId) {
    await commit(types.TM_PROJECT_DELETE_DELIVERY, {
      deliveryId: deliveryId
    });
  },
  async getAll ({state, dispatch, commit}, force) {
    if (!state.items || state.items.length === 0 || force) {
      dispatch('app/loadingDataEnable', null, {root: true});
      const tmProjects = await tmProjectService.findAll();
      await commit(types.TM_PROJECT_LIST, {
        items: tmProjects
      });
      dispatch('app/loadingDataDisable', null, {root: true});
    }
  },
  async create ({ dispatch, commit}, tmProject) {
    try {
      let response = await tmProjectService.create(tmProject);
      if (response.ok) {
        await dispatch('getAll', true);
        return true;
      } else {
        return false;
      }
    } catch (ex) {
      console.error(ex);
    }
  },
  async update ({ dispatch, commit}, tmProject) {
    try {
      let response = await tmProjectService.update(tmProject);
      if (response.ok) {
        await dispatch('getAll', true);
        return true;
      } else {
        return false;
      }
    } catch (ex) {
      console.error(ex);
    }
  },
  async delete ({dispatch, commit}, id) {
    let response = await tmProjectService.delete(id);
    if (response.ok) {
      dispatch('getAll', true);
      return true;
    } else {
      return false;
    }
  }
};

export default actions
