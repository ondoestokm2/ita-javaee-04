import actions from './userActions';
import mutations from './userMutations';

const state = {
  items: []
};

export default {
  namespaced: true,
  state,
  mutations: mutations,
  actions: actions
}
