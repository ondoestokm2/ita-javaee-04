import * as types from "../../mutationTypes";
import userService from "../../../services/userService";

const actions = {
  async getAll ({ state, dispatch, commit}, force) {
    if (!state.items || state.items.length === 0 || force) {
      dispatch('app/loadingDataEnable', null, {root: true});
      const users = await userService.findAll();
      await commit(types.USER_LIST, {
        items: users
      });
      dispatch('app/loadingDataDisable', null, {root: true});
    }
  },
  async create ({ dispatch, commit}, user) {
    let response = await userService.create(user);
    dispatch('getAll', true);
  },
  async update ({ dispatch, commit}, user) {
    let response = await userService.update(user);
    dispatch('getAll', true);
  },
  async activate ({ dispatch, commit}, id) {
    let response = await userService.activate(id);
    dispatch('getAll', true);
  },
  async deactivate ({ dispatch, commit}, id) {
    let response = await userService.deactivate(id);
    dispatch('getAll', true);
  },
  async delete ({dispatch, commit}, id) {
    let response = await userService.delete(id);
    if (response.ok) {
      dispatch('getAll', true);
      return true;
    } else {
      return false;
    }
  },
  async getAllFailed ({commit}) {
    await commit(types.USER_LIST, {
      items: []
    });
  },
  async updateProfile ({dispatch, commit}, user) {
    try {
      let response = await userService.updateMe(user);
      if (response.ok) {
        return await dispatch('auth/fetchMe', null, { root: true })
      } else {
        return false;
      }
    } catch (ex) {
      console.error(ex);
    }
  },
  async myPasswordChange ({}, credentials) {
    try {
      let response = await userService.myPasswordChange(credentials);
      return response.ok;
    } catch (ex) {
      console.error(ex);
    }
  },
  async passwordChange ({}, data) {
    try {
      let response = await userService.passwordChange(data.userId, data.credentials);
      return response.ok;
    }
     catch (ex) {
      console.error(ex);
    }
  }
};

export default actions
