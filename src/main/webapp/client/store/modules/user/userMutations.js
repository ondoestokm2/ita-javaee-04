import { USER_LIST } from '../../mutationTypes'

const mutations = {
  [USER_LIST](state, action){
    state.items = action.items
  }
};

export default mutations;
