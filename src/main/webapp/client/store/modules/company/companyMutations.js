import { COMPANY_LIST, COMPANY_ARES_LIST, COMPANY_ACTIVATE, COMPANY_DEACTIVATE } from '../../mutationTypes'
import _ from 'lodash'

const mutations = {
  [COMPANY_LIST](state, action){
    state.items = action.items
  },
  [COMPANY_ARES_LIST](state, action){
    state.aresItems = action.aresItems
  },
  [COMPANY_ACTIVATE](state, id){
    _.find(state.items, {id: id}).active = true;
  },
  [COMPANY_DEACTIVATE](state, id){
    _.find(state.items, {id: id}).active = false;
  }
};

export default mutations;
