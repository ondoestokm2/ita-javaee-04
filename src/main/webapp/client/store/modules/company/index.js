import actions from './companyActions';
import getters from './companyGetters';
import mutations from './companyMutations';

const state = {
  items: [],
  aresItems: []
};

export default {
  namespaced: true,
  state,
  getters: getters,
  mutations: mutations,
  actions: actions
}
