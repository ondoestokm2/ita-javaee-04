import actions from './tmDeliveryActions';
import mutations from './tmDeliveryMutations';

const state = {
};

export default {
  namespaced: true,
  state,
  mutations: mutations,
  actions: actions
}
