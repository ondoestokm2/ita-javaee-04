import { APP_CONFIGURATION_LOAD, APP_SUBMIT_PROTECTION_ENABLE, APP_SUBMIT_PROTECTION_DISABLE, APP_LOADING_DATA_ENABLE, APP_LOADING_DATA_DISABLE } from '../../mutationTypes';

const mutations = {
  async [APP_CONFIGURATION_LOAD](state, action){
    state.configuration = action.configuration;
  },
  async [APP_SUBMIT_PROTECTION_ENABLE](state){
    state.submitProtection = true;
  },
  async [APP_SUBMIT_PROTECTION_DISABLE](state){
    state.submitProtection = false;
  },
  async [APP_LOADING_DATA_ENABLE](state){
    state.loadingData = true;
  },
  async [APP_LOADING_DATA_DISABLE](state){
    state.loadingData = false;
  }
};

export default mutations;
